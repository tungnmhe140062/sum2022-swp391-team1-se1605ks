using System;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using Project.Models;
using System.Linq;
using Microsoft.Ajax.Utilities;

namespace Project.Filter
{
    public class AuthenticationFilter: ActionFilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            ProjectEntities db = new ProjectEntities();
            
            if (filterContext.HttpContext.Session["userId"] == null)
            {
                filterContext.Result = new RedirectResult("/");
                return;
            }
            
            var userId = Int32.Parse(filterContext.HttpContext.Session["userId"].ToString());
            var listAccount = from e in db.Accounts where e.Id == userId select e;

            if (listAccount.ToList().Count == 0)
            {
                filterContext.Result = new RedirectResult("/");
                filterContext.HttpContext.Session.Clear();
                return;
            }
            
            filterContext.HttpContext.Session["role"] = listAccount.ToList()[0].Role;
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            
        }
    }
}