using System;
using System.Linq;
using System.Web.Mvc;

namespace Project.Filter
{
    public class AuthorizationFilter : ActionFilterAttribute
    {
        private string[] Role { get; set; }

        public AuthorizationFilter(string role)
        {
            Role = role.Split(';');
            Console.Out.WriteLine(role);
        }
        
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            string[] arrayRole = { "customer", "sale", "sale manager", "marketing", "admin" };
            if (!Role.Contains(arrayRole[Int32.Parse(filterContext.HttpContext.Session["role"].ToString())]))
            {
                filterContext.Result = new RedirectResult("/");
            }
        }
    }
}