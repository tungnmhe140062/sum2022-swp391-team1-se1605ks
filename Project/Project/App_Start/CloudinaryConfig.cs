using System.Web.Configuration;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;

namespace Project
{
    public static class CloudinaryConfig
    {
        public static string[] Upload(string[] paths)
        {
            // get account from cloudinary
            Account a = new Account(
                WebConfigurationManager.AppSettings["cloudinary_cloud"],
                WebConfigurationManager.AppSettings["cloudinary_api_key"],
                WebConfigurationManager.AppSettings["cloudinary_api_secret"]);

            Cloudinary cloudinary = new Cloudinary(a);
            cloudinary.Api.Secure = true;

            // Upload images to cloudinary
            string[] arrayString = new string[paths.Length];
            for (int i = 0; i < paths.Length; i++)
            {
                var uploadResult = cloudinary.Upload(new ImageUploadParams()
                {
                    File = new FileDescription(paths[i]),
                });
                arrayString[i] = uploadResult.SecureUrl.ToString();
            }
            
            // return array paths image (secure url)
            return arrayString;
        }
    }
}