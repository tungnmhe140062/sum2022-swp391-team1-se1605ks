using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Project
{
    public static class SendGridConfig
    {
        private static async Task SendEmail(string email, string htmlContent, string subject)
        {
            // Config sendgrid - get api key
            var apiKey = WebConfigurationManager.AppSettings["sendgrid_api_key"];
            var client = new SendGridClient(apiKey);
            
            // setup Sender (Warning: Don't Change)
            var from = new EmailAddress("admin@nmtung.xyz", "SWP391 - OnlineShop");
            
            var to = new EmailAddress(email);
            
            var msg = MailHelper.CreateSingleEmail(from, to, subject, null, htmlContent);
            
            // action send email
            var response = await client.SendEmailAsync(msg).ConfigureAwait(false);
        }

        public static void SendEmailActive(string email, string code)
        {
            string subject = "Active your account";
            string content = "Link for active your account here: <a href='https://projectswp.azurewebsites.net/Auth/Active?code=" + code + "'>Here<a/>";

            SendEmail(email, content, subject);
        }

        public static void SendEmailResetPass(string email, string code)
        {
            string subject = "Reset Password";
            string content = "Link for reset your account's password here: <a href='https://projectswp.azurewebsites.net/ResetPassword/Forget?token=" + code + "'>Here<a/>";

            SendEmail(email, content, subject);
        }
    }
}