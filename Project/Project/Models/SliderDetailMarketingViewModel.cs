using Project.Models.manage;

namespace Project.Models
{
    public class SliderDetailMarketingViewModel : MarketingCommonViewModel
    {
        public Slider SelectSlider { get; set; }
    }
}