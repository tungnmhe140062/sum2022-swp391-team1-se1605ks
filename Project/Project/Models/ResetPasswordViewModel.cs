﻿using Project.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class ResetPasswordViewModel : HomeViewLayout
    {
        public String email { get; set; }
        public String messageError { get; set; }
        public String messageSuccess { get; set; }
    }
}