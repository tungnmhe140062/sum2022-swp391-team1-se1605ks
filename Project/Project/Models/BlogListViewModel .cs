﻿using Project.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class BlogListViewModel:HomeViewLayout
    {
        public int CountList { get; set; }
        public IEnumerable<Blog> BlogList { get; set; }
        public Blog blog { get; set; }
        public Account Author { get; set; }
        public Dictionary<Blog,Account> LastestPost { get; set; }
        public Dictionary<Blog,Account> TrendingPost { get; set; }
    }
}