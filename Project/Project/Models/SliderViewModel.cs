﻿using Project.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Project.Models.manage;

namespace Project.Models
{
    public class SliderViewModel : MarketingCommonViewModel
    {
        public Account Account { get; set; }
        public List<Slider> Slider { get; set; }
        public int CountList { get; set; }
        public Slider slider { get; set; }
        public string filter { get; set; }
        public int Current_Page { get; set; }

        public enum TabEnumSlider
        {
            Dashboard,
            OrderList,
            OrderDetail
        }
    }
}