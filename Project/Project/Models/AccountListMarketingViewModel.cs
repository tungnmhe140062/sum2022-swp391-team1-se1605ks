using System.Collections.Generic;
using Project.Models.manage;

namespace Project.Models
{
    public class AccountListMarketingViewModel : MarketingCommonViewModel
    {
        public List<Account> ListAccount { get; set; }
        public string QueryName { get; set; }
        public string QueryEmail { get; set; }
        public string QueryPhone { get; set; }
        public int Page { get; set; }
        public int TotalPage { get; set; }
        public int SortBy { get; set; }
    }
}