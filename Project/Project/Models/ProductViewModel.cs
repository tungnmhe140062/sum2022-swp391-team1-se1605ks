﻿using Project.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class ProductViewModel:HomeViewLayout
    {
        public Product MainProduct { get; set; }    
        public IEnumerable<Product> Products { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<ProductCategory> ProductCategory { get; set; }
        public IEnumerable<ImageProduct> ImageProducts { get; set; }
    }
}