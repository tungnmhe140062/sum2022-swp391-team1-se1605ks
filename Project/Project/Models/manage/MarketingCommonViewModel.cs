namespace Project.Models.manage
{
    public class MarketingCommonViewModel 
    {
        public string Title { get; set; } = "(Set title for view model)";
        public TabEnumMkt Tab { get; set; }
    }

    public enum TabEnumMkt
    {
        Dashboard,
        Customer,
        Feedback,
        Post,
        Product,
        Slider,
    }
}