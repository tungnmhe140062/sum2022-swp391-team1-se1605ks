using Project.Controllers;
using System;
using System.Collections.Generic;

namespace Project.Models.manage
{
    public class SaleCommonViewModel : HomeViewLayout
    {
        //Order
        public Dictionary<OrderDetail,Product> Order_detail { get; set; }
        public Dictionary<Order,int> OrderList { get; set; }       
        public Order Order { get; set; }
        public int CountOrder { get; set; }
        public int SortBy { get; set; }
        public string filter { get; set; }
        public int current_page { get; set; }
        public string param { get; set; }
        public List<Account> seller_list { get; set; }
        public string SellerName { get; set; }
        //sale dashboard
        public int CountOrderSuccess { get; set; }
        public int TotalSales { get; set; }
        public string Last7Days { get; set; }
        public string Last7DaysData { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string message { get; set; }
        public int status_id { get; set; }
        public string status_name { get; set; }
        public string Title { get; set; } = "(Set title for view model)";
        public TabEnumSale Tab { get; set; }

    }

    public enum TabEnumSale
    {
        Dashboard,
        OrderList,
        OrderDetail
    }
}
