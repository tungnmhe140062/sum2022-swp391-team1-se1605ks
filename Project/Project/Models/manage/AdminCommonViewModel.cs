namespace Project.Models.manage
{
    public class AdminCommonViewModel
    {
        public string Title { get; set; } = "(Set title for view model)";
        public TabEnumAdmin Tab { get; set; }
    }

    public enum TabEnumAdmin
    {
        Dashboard,
        Customer,
        Feedback,
        Post,
        Product,
        Slider,
    }
}