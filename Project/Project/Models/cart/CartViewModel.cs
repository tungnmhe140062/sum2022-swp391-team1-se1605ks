using System.Collections.Generic;
using Project.Controllers;

namespace Project.Models.cart
{
    public class CartViewModel : HomeViewLayout
    {
        public List<Cart> Cart { get; set; }
        public double Total { get; set; }
    }
}