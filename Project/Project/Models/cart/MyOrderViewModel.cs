using System.Collections.Generic;
using Project.Controllers;

namespace Project.Models.cart
{
    public class MyOrderViewModel : HomeViewLayout
    {
        public bool IsEmpty { get; set; }
        public List<Order> Orders { get; set; }
        public Order SelectOrder { get; set; }
        public int SelectedOrderId { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
    }
}