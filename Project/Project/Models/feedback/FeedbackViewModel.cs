using System.Collections.Generic;
using Project.Controllers;

namespace Project.Models.feedback
{
    public class FeedbackViewModel : HomeViewLayout
    {
        public bool IsEmpty { get; set; }
        public bool NotFeedback { get; set; }
        public List<OrderDetail> ListOrderDetails { get; set; }
        public Feedback Feedback { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public OrderDetail SelectOrderDetail { get; set; }
    }
}