using System.Collections.Generic;
using Project.Models.manage;

namespace Project.Models
{
    public class FeedbackMarketingViewModel : MarketingCommonViewModel
    {
        public List<Feedback> ListFeedback { get; set; }
        public int Page { get; set; }
        public int TotalPage { get; set; }
        public int SortBy { get; set; }
        public string Query { get; set; }
    }
}