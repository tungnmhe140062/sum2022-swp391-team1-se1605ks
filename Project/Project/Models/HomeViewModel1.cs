﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Project.Controllers;

namespace Project.Models
{
    public class HomeViewModel1:HomeViewLayout
    {
        public IEnumerable<Product> Products { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<ProductCategory> ProductCategory { get; set; }
        public Blog NewBlog { get; set; }
        public IEnumerable<Slider> Slider { get; set; }

    }
}