﻿using Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Controllers
{
    public class ChangePasswordController : Controller
    {
        // GET: ChangePassword
        private ProjectEntities db = new ProjectEntities();
        // GET: ChangePassword
        [HttpPost]
        public ActionResult ChangePass(string oldPassword, string newPassword, string cfirmPassword)

        {
            Console.Out.WriteLine(oldPassword);
            Console.Out.WriteLine(newPassword);

            var userLogged = (from e in db.Accounts where e.Id == 1 select e).ToList()[0];

            if (newPassword == cfirmPassword)
            {
                userLogged.Password = newPassword;
                db.SaveChanges();
            }

            return null;
        }
    }
}