﻿using Project.Filter;
using Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Controllers
{
    public class ProductMKTController : Controller
    {
        ProjectEntities db = new ProjectEntities();
        // GET: ProductMKT
        /*[AuthenticationFilter]
        [AuthorizationFilter("marketing")]*/

        public ActionResult GetProductListMKT(int? id)
        {
            
            int count = db.Products.ToList().Count;
            ViewBag.count = count;
            List <ProductMKTViewModel> list = new List<ProductMKTViewModel>();
            if (id != null && id !=1)
            {
                HttpContext.Session["ProductListMKTIndex"] = id;
                var lp = db.Products.OrderBy(i=>0).Skip((id.GetValueOrDefault() - 1) * 9).Take(9).ToList();
                if(lp.Count == 0)
                {
                    return HttpNotFound();
                }
                var listCateId = db.Products.Join(db.ProductCategories, p => p.Id, c => c.ProductId, (p, c) => new
                {
                    ProductId = p.Id,
                    CateId = c.CategoryId
                }).Join(db.Categories, lc => lc.CateId, c => c.Id, (lc, c) => new
                {
                    ProductId = lc.ProductId,
                    CateId = lc.CateId,
                    Title = c.Title,
                    Detail = c.Detail

                });
                var listImage = db.Products.Join(db.ImageProducts, p => p.Id, i => i.ProductId, (p, i) => new
                {
                    ProductId = p.Id,
                    Image = i.Image
                });


                foreach (var item in lp)
                {
                    List<string> CateGoriesDetails = new List<string>();
                    List<string> Images = new List<string>();
                    var lc = listCateId.Where(o => o.ProductId == item.Id).ToList();
                    foreach (var item1 in lc)
                    {
                        CateGoriesDetails.Add(item1.Detail);
                    }

                    var li = listImage.Where(o => o.ProductId == item.Id).ToList();
                    foreach (var item1 in li)
                    {
                        Images.Add(item1.Image);
                    }
                    list.Add(new ProductMKTViewModel(item, CateGoriesDetails, Images));
                }
                

            }
            else if(id==null || id ==1)
            {
                HttpContext.Session["ProductListMKTIndex"] = 1;
                var lp = db.Products.Take(9).ToList();
                if (lp.Count == 0)
                {
                    return HttpNotFound();
                }
                var listCateId = db.Products.Join(db.ProductCategories, p => p.Id, c => c.ProductId, (p, c) => new
                {
                    ProductId = p.Id,
                    CateId = c.CategoryId
                }).Join(db.Categories, lc => lc.CateId, c => c.Id, (lc, c) => new
                {
                    ProductId = lc.ProductId,
                    CateId = lc.CateId,
                    Title = c.Title,
                    Detail = c.Detail

                });
                var listImage = db.Products.Join(db.ImageProducts, p => p.Id, i => i.ProductId, (p, i) => new
                {
                    ProductId = p.Id,
                    Image = i.Image
                }); 


                foreach (var item in lp)
                {
                    List<string> CateGoriesDetails = new List<string>();
                    List<string> Images = new List<string>();
                    var lc = listCateId.Where(o => o.ProductId == item.Id).ToList();
                    foreach(var item1 in lc)
                    {
                        CateGoriesDetails.Add(item1.Detail);
                    }
                    
                    var li = listImage.Where(o => o.ProductId == item.Id).ToList();
                    foreach (var item1 in li)
                    {
                        Images.Add(item1.Image);
                    }
                    list.Add(new ProductMKTViewModel(item, CateGoriesDetails, Images));
                }
               
            }

            ViewBag.Index = id;
            return View(new ListProductMKTViewModel(list));
            
        }
       

    }
    public class ProductMKTViewModel { 
        public Product Product { get; set; }
        public List<string> CateGories { get; set; }
        public List<string> Images { get; set; }

        public ProductMKTViewModel(Product product, List<string> cateGories, List<string> images)
        {
            Product = product;
            CateGories = cateGories;
            Images = images;
        }
    }
    public class ListProductMKTViewModel : HomeViewLayout
    {
        public List<ProductMKTViewModel> listPVM { get; set; }

        public ListProductMKTViewModel(List<ProductMKTViewModel> listPVM)
        {
            this.listPVM = listPVM;
        }
    }

}
