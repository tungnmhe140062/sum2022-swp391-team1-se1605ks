using System;
using System.IO;
using System.Web.Mvc;
using Project.Filter;
using Project.Models;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Project.Controllers
{
    public class AccountController : Controller
    {
        private ProjectEntities db = new ProjectEntities();
        
        [HttpPost]
        [AuthenticationFilter]
        [AuthorizationFilter("customer")]
        public ActionResult ChangeInformation(
            HttpPostedFileBase image,
            string name,
            string gender,
            string mobile,
            string address)
        {
            int userId = Int32.Parse(Session["userId"].ToString());
            
            // check phone number
            bool isValidPhone = Regex.IsMatch(mobile, @"^[0-9]{10}$");
            if (!isValidPhone)
            {
                TempData["isInvalidPhoneRegister"] = true;
                return RedirectToAction("Index", "Home");
            }
            
            // get account by userId
            var selectAccount = (from account in db.Accounts where account.Id == userId select account).ToList()[0];

            if (image != null)
            {
                // save image to temp folder
                string newFileName = DateTime.Now.Ticks + "_" + image.FileName;
                string path = Path.Combine(Server.MapPath("~/TempImageUpload"), newFileName);
                image.SaveAs(path);

                // upload to cloudinary and get url of image
                string[] url = CloudinaryConfig.Upload(new string[] { path });
                
                // delete image from temp folder
                System.IO.File.Delete(path);
                
                selectAccount.Image = url[0];
            }
            
            selectAccount.FullName = name;
            selectAccount.Gender = Int32.Parse(gender);
            selectAccount.Phone = mobile;
            selectAccount.Address = address;

            db.SaveChanges();
            
            return RedirectToAction("Index", "Home");
        }
    }
}