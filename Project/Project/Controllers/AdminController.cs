﻿using Project.Filter;
using Project.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Project.Models.manage;
namespace Project.Controllers
{

    public class RevenuesOfCategory
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public int Revenue { get; set; }
    }
    public class HotTrend
    {
        public int CategoryId { get; set; }
        public string ProductName { get; set; }

        public int Count { get; set; }
    }
    public class FeedbackByCategory
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public double RateStar { get; set; }
    }
    public class AdminModel : AdminCommonViewModel
    {
        public Account Account { get; set; }
        public int OrderSuccess { get; set; }
        public int OrderCancel { get; set; }
        public int OrderSubmit { get; set; }
        public int TotalRevenues { get; set; }
        public IEnumerable<RevenuesOfCategory> RevenuesCategories { get; set; }
        public double AverageStar { get; set; }
        public int NewAccount { get; set; }
        public IEnumerable<Account> NewMember { get; set; }
        public HotTrend Trend { get; set; }

        public IEnumerable<FeedbackByCategory> FeedbackbyCategories { get; set; }
    }


    [AuthenticationFilter]
    [AuthorizationFilter(role: "admin")]
    public class AdminController : Controller
    {
        private ProjectEntities db = new ProjectEntities();
        // GET: Admin
       
        public ActionResult Dashboard(DateTime? from, DateTime? to)
        {
            // to = DateTime.Now;
            //  from = (DateTime.Now.Date - 7);
            AdminModel adminmodel = new AdminModel();
            adminmodel.Title = "Dashboard";
            adminmodel.Tab = TabEnumAdmin.Dashboard;
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                adminmodel.Account = selectAccount;

            }


            if (from != null && to != null)
            {
                adminmodel.NewAccount = db.Accounts.Where(a => a.CreatedAt >= from && a.CreatedAt <= to).Count(a => a.IsActive == true);
                adminmodel.OrderSuccess = db.Orders.Where(a => a.CreatedAt >= from && a.CreatedAt <= to).Count(os => os.Status == 1);
                adminmodel.OrderCancel = db.Orders.Where(a => a.CreatedAt >= from && a.CreatedAt <= to).Count(oc => oc.Status == 0);
                adminmodel.OrderSubmit = db.Orders.Where(a => a.CreatedAt >= from && a.CreatedAt <= to).Count(os => os.Status == 2);

                adminmodel.TotalRevenues = (db.OrderDetails.Where(a => a.CreatedAt >= from && a.CreatedAt <= to)).Sum(a => a.SalePrice);

                adminmodel.AverageStar = db.Feedbacks.Average(a => a.RateStar);
                adminmodel.NewMember = db.Accounts.OrderByDescending(a => a.Id).Where(a => a.IsActive == true && a.Role == 0 && a.CreatedAt >= from && a.CreatedAt <= to).Take(5);
                adminmodel.RevenuesCategories = (from s in db.OrderDetails // outer sequence
                                                 join c in db.ProductCategories//inner sequence 
                                                 on s.ProductId equals c.ProductId
                                                 group c by c.CategoryId into r
                                                 select new RevenuesOfCategory
                                                 {
                                                     CategoryName = r.FirstOrDefault().Category.Title == null ? "Co the la null" : r.FirstOrDefault().Category.Title,
                                                     Revenue = r.Sum(s => s.Product.SalePrice)


                                                 }).ToList();
                adminmodel.Trend = db.OrderDetails.GroupBy(o => o.ProductId).Select(group => new HotTrend
                { ProductName = group.FirstOrDefault().Product.Title, Count = group.Count() }).
                 OrderByDescending(x => x.Count).FirstOrDefault();
            }
            else
            {

                adminmodel.NewAccount = db.Accounts.Count(a => a.IsActive == true);
                adminmodel.OrderSuccess = db.Orders.Count(os => os.Status == 1);
                adminmodel.OrderCancel = db.Orders.Count(oc => oc.Status == 0);
                adminmodel.OrderSubmit = db.Orders.Count(os => os.Status == 2);
                adminmodel.TotalRevenues = db.OrderDetails.Sum(t => t.SalePrice);
                adminmodel.AverageStar = db.Feedbacks.Average(a => a.RateStar);
                adminmodel.NewMember = db.Accounts.OrderByDescending(a => a.Id).Where(a => a.IsActive == true && a.Role == 0).Take(5);
                adminmodel.RevenuesCategories = (from s in db.OrderDetails // outer sequence
                                                 join c in db.ProductCategories//inner sequence 
                                                 on s.ProductId equals c.ProductId
                                                 group c by c.CategoryId into r
                                                 select new RevenuesOfCategory
                                                 {
                                                     CategoryId = r.Key,
                                                     CategoryName = r.FirstOrDefault().Category.Title == null ? "Co the la null" : r.FirstOrDefault().Category.Title,
                                                     Revenue = r.Sum(s => s.Product.SalePrice)


                                                 }).ToList();

                adminmodel.Trend = db.OrderDetails.GroupBy(o => o.ProductId).Select(group => new HotTrend
                { ProductName = group.FirstOrDefault().Product.Title, Count = group.Count() }).
                 OrderByDescending(x => x.Count).FirstOrDefault();
            }


            //  var cate = db.Categories.ToList();
            /* foreach(var c in cate)
             {
                 adminmodel.RevenuesCategories = db.OrderDetails.Sum(rs => rs.SalePrice);
             }*/

            return View(adminmodel);
        }

        [AuthenticationFilter]
        [AuthorizationFilter(role: "admin")]
        public ActionResult UserList()
        {
            UserListModel userModel = new UserListModel();
            userModel.Title = "Manage User";
            userModel.Tab = TabEnumAdmin.Customer;
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                userModel.Account = selectAccount;
            }

            userModel.accounts = db.Accounts.ToList();


            return View(userModel);
        }

        [AuthenticationFilter]
        [AuthorizationFilter(role: "admin")]
        public ActionResult UserDetail(int? id)
        {
            UserListModel userDetailModel = new UserListModel();

            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                userDetailModel.Account = selectAccount;
            }

            userDetailModel.accounts = db.Accounts.Where(a => a.Id == id);

            return View(userDetailModel);
        }
    }
}