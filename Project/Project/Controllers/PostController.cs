﻿using Project.Filter;
using Project.Models;
using Project.Models.manage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Controllers
{
    public class PostController : Controller
    {
        ProjectEntities db = new ProjectEntities();
        // GET: Post
        /*[AuthenticationFilter]
        [AuthorizationFilter("marketing")]*/
        public ActionResult List(int? id)
        {
            PostListViewModel plvmodel = new PostListViewModel();
            if (id == null)
            {
                var authorName = db.Blogs.Join(db.Accounts, (b => b.AuthorId), (a => a.Id), (b, a) => new PostViewModel
                {
                    Id = b.Id,
                    AuthorName = a.FullName,
                    Title = b.Title,
                    Image = b.Image,
                    Detail = b.Detail,
                    CountView = b.CountView
                }).Take(7).ToList();
                plvmodel.Posts = authorName;
            }
            else
            {
                var authorName = db.Blogs.Join(db.Accounts, (b => b.AuthorId), (a => a.Id), (b, a) => new PostViewModel
                {
                    Id = b.Id,
                    AuthorName = a.FullName,
                    Title = b.Title,
                    Image = b.Image,
                    Detail = b.Detail,
                    CountView = b.CountView
                }).OrderBy(i => 0).Skip((id.GetValueOrDefault() - 1) * 7).Take(7).ToList();
                plvmodel.Posts = authorName;
            }
            plvmodel.Authors = db.Accounts.ToList();
            if (id == null || id == 1)
            {
                ViewBag.Index = 1;
            }
            else ViewBag.Index = id;

            ViewBag.count = db.Blogs.Count();

            return View(plvmodel);
        }
        [HttpGet]
        [Route("Post/Delete/{id:int}/{index:int}")]

        public ActionResult Delete(int id, int index)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            else
            {
                var blog = db.Blogs.Where(o => o.Id == id).FirstOrDefault();
                if (blog == null)
                {
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
                }
                else
                {
                    db.Blogs.Attach(blog);
                    db.Blogs.Remove(blog);
                    db.SaveChanges();
                }
                return RedirectToAction("List", "Post", new { id = index });
            }
        }

        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            Blog blog = new Blog();
            blog.Title = HttpContext.Request.Form["Title"];
            blog.Detail = HttpContext.Request.Form["Detail"];
            blog.AuthorId = Int32.Parse(HttpContext.Request.Form["Author"]);
            var filebase = (HttpPostedFileBase)HttpContext.Request.Files["File"];
            if (filebase != null)
            {
                // save image to temp folder
                string newFileName = DateTime.Now.Ticks + "_" + filebase.FileName;
                string path = Path.Combine(Server.MapPath("~/TempImageUpload"), newFileName);
                filebase.SaveAs(path);

                // upload to cloudinary and get url of image
                string[] url = CloudinaryConfig.Upload(new string[] { path });

                // delete image from temp folder
                System.IO.File.Delete(path);

                blog.Image = url[0];
            }
            blog.CreatedAt = DateTime.Now.Date;
            blog.UpdatedAt = DateTime.Now.Date;
            db.Blogs.Add(blog);
            db.SaveChanges();
            TempData["successmessage"] = "Create post successfully";
            return RedirectToAction("List", "Post");
        }
    }



    public class PostViewModel
    {
        public int Id { get; set; }
        public string AuthorName { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string Detail { get; set; }
        public int CountView { get; set; }
    }

    public class PostListViewModel : MarketingCommonViewModel
    {
        public IEnumerable<PostViewModel> Posts { get; set; }
        public IEnumerable<Account> Authors { get; set; }
    }
}