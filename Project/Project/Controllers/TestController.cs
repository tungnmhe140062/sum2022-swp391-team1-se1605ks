using System;
using System.Linq;
using System.Web.Mvc;
using Project.Filter;
using Project.Models;

namespace Project.Controllers
{
    public class TestController : Controller
    {
        private ProjectEntities db = new ProjectEntities();
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddSession()
        {
            Session["userId"] = 1;
            return View();
        }
        
        public ActionResult DeleteSession()
        {
            Session.Clear();

            return RedirectToAction("Index", "Home");
        }

        [AuthenticationFilter]
        public ActionResult GetInformation()
        {
            var t = 
                from e in db.Accounts where e.Id == 1 select e;
            
            var a = t.ToList();
            Console.Out.WriteLine(a[0].Role);
            
            return RedirectToAction("Index", "Home");
        }

        [AuthenticationFilter]
        [AuthorizationFilter(role:"customer;admin")]
        public ActionResult CheckAuth()
        {
            return View();
        }
    }
}