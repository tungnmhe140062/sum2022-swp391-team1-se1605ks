﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class MyOrderController : Controller
    {
        private ProjectEntities db = new ProjectEntities();
        
        // GET: MyOrder
        public ActionResult MyOrder()
        {
            HomeViewLayout hvl = new HomeViewLayout();
            
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                hvl.Account = selectAccount;
            }
            
            return View(hvl);
        }
    }
}