using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using Project.Models;
using System.Linq;
using System.Web;
using Project.Filter;
using Project.Models.feedback;

namespace Project.Controllers
{
    [AuthenticationFilter]
    [AuthorizationFilter(role: "customer")]
    public class FeedbackController : Controller
    {
        private ProjectEntities db = new ProjectEntities();

        public ActionResult Index(string orderId, string productId)
        {
            // init feedback model and get userId
            FeedbackViewModel layout = new FeedbackViewModel();
            int userid = 0;
            if (Session["userId"] != null)
            {
                userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                layout.Account = selectAccount;
            }

            userid = Int32.Parse(Session["userId"].ToString());

            // check if orderId and productId is null
            if (orderId == null || productId == null)
            {
                var listOrders =
                    (from order in db.Orders where order.AccountId == userid select order).OrderByDescending(e =>
                        e.CreatedAt);

                // check don't have order
                layout.IsEmpty = false;
                if (listOrders.ToList().Count == 0)
                {
                    layout.IsEmpty = true;
                    return View(layout);
                }
                else
                {
                    // get orderId and productId newest
                    var newest = listOrders.ToList()[0].OrderDetails.ToList()[0];
                    return Redirect(
                        $"/feedback?orderId={newest.OrderId}&productId={newest.ProductId}");
                }
            }
            else
            {
                // Parse orderId and productId
                int orderIdN = Int32.Parse(orderId);
                int productIdN = Int32.Parse(productId);

                // get orderDetail
                var temp = (from e in db.OrderDetails
                    where e.OrderId == orderIdN && e.ProductId == productIdN
                    select e).ToList()[0];

                layout.NotFeedback = temp.FeedbackId == null;
                if (temp.FeedbackId != null)
                {
                    var selectFeedback = temp.Feedback;
                    layout.Feedback = selectFeedback;
                }

                layout.OrderId = orderIdN;
                layout.ProductId = productIdN;

                var selectOrderDetail = (from e in db.OrderDetails
                    where e.OrderId == orderIdN && e.ProductId == productIdN
                    select e).ToList()[0];

                layout.SelectOrderDetail = selectOrderDetail;

                // get list order
                var listOrders =
                    (from order in db.Orders where order.AccountId == userid select order).OrderByDescending(e =>
                        e.CreatedAt);

                List<OrderDetail> listOrderDetails = new List<OrderDetail>();

                Array.ForEach(listOrders.ToArray(),
                    order =>
                        listOrderDetails.AddRange(order.OrderDetails.OrderByDescending(e => e.CreatedAt).ToList()));
                layout.ListOrderDetails = listOrderDetails;
            }

            return View(layout);
        }

        [HttpPost]
        public ActionResult SubmitFeedback(int productId, int orderId, int rate, string note, HttpPostedFileBase[] images)
        {
            List<string> listImage = new List<string>();
            foreach (var image in images)
            {
                // save image to temp folder
                string newFileName = DateTime.Now.Ticks + "_" + image.FileName;
                string path = Path.Combine(Server.MapPath("~/TempImageUpload"), newFileName);
                image.SaveAs(path);
                listImage.Add(path);
            }

            // upload to cloudinary and get url of image
            string[] urls = CloudinaryConfig.Upload(listImage.ToArray());

            // delete image from temp folder
            foreach (var path in listImage)
            {
                System.IO.File.Delete(path);
            }

            var newFeedback = db.Feedbacks.Add(new Feedback()
            {
                RateStar = rate,
                Detail = note,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
            });
            
            db.SaveChanges();

            var selectOrderDetail = db.OrderDetails.FirstOrDefault(e => e.OrderId == orderId && e.ProductId == productId);
            selectOrderDetail.FeedbackId = newFeedback.Id;
            selectOrderDetail.UpdatedAt = DateTime.Now;
            
            List<ImageFeedback> listImageFeedback = new List<ImageFeedback>();

            foreach (var url in urls)
            {
                listImageFeedback.Add(new ImageFeedback()
                {
                    Image = url,
                    FeedbackId = newFeedback.Id,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                });
            }
            
            db.ImageFeedbacks.AddRange(listImageFeedback);

            db.SaveChanges();

            return Redirect($"/feedback?orderId={orderId}&productId={productId}");
        }
    }
}