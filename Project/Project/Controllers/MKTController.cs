﻿using Project.Filter;
using Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.Models.manage;
using System.IO;

namespace Project.Controllers
{
    public class RevenuesOfCategory1
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int Revenue { get; set; }
    }
    public class MKTModel : MarketingCommonViewModel
    {
        public int Posts { get; set; }
        public int Product { get; set; }
        public int Feedback { get; set; }
        public int TotalRevenues { get; set; }
        public IEnumerable<RevenuesOfCategory> RevenuesCategories { get; set; }
        public double AverageStar { get; set; }
        public int NewAccount { get; set; }
        public IEnumerable<Account> NewMember { get; set; }
        public HotTrend Trend { get; set; }
    }

    public class PostViewModelz
    {
        public int Id { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public string AuthorName { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string Detail { get; set; }
        public int CountView { get; set; }
    }

    public class MKTPost : MarketingCommonViewModel
    {
        public PostViewModelz PostDetail { get; set; }
        public Account Author { get; set; }

        public Blog EditPost { get; set; }
    }
    public class MKTProductDetail : MarketingCommonViewModel
    {
        public Product Products { get; set; }
        public Category Categories { get; set; }
        public IEnumerable<ProductCategory> ProductCategory { get; set; }
        public IEnumerable<ImageProduct> ImageProducts { get; set; }
    }
    public class PostListViewModelz : MarketingCommonViewModel
    {
        public IEnumerable<PostViewModelz> Posts { get; set; }
        public IEnumerable<Account> Authors { get; set; }
    }
    public class MKTCustomer : MarketingCommonViewModel
    {
        public Account Customer { get; set; }
    }

    public class MKTFeedbackDetail : MarketingCommonViewModel
    {
        public Feedback Feedback { get; set; }
        public ImageFeedback ImageFeedback { get; set; }
        public Account CustomerFeedback { get; set; }
        public Product FeedbackProductImage { get; set; }
    }

   
    public class MKTProductDetailz 
    {
        public Product Product { get; set; }
        public List<string> Images { get; set; }
        public List<Category> Categories { get; set; }

        public MKTProductDetailz(Product product, List<string> images, List<Category> categories)
        {
            Product = product;
            Images = images;
            Categories = categories;
        }
    }
   
    public class MKTProductDetailList : MarketingCommonViewModel
    {
        public List<MKTProductDetailz> MKTProductDetails { get; set; }

        public MKTProductDetailList()
        {
        }

        public MKTProductDetailList(List<MKTProductDetailz> mKTProductDetails)
        {
            MKTProductDetails = mKTProductDetails;
        }
    }

    [AuthenticationFilter]
    [AuthorizationFilter(role: "marketing")]
    public class MKTController : Controller
    {
        private ProjectEntities db = new ProjectEntities();
        // GET: MKT
        [AuthenticationFilter]
        [AuthorizationFilter(role: "marketing")]
        public ActionResult Dashboard(DateTime? from, DateTime? to)
        {
            MKTModel mktmodel = new MKTModel();
            mktmodel.Title = "Dashboard";
            mktmodel.Tab = TabEnumMkt.Dashboard;

            if (from != null && to != null)
            {
                mktmodel.NewAccount = db.Accounts.Where(a => a.CreatedAt >= from && a.CreatedAt <= to).Count(a => a.IsActive == true);
                mktmodel.Posts = db.Sliders.Where(a => a.CreatedAt >= from && a.CreatedAt <= to).Count();
                mktmodel.Product = db.Products.Where(a => a.CreatedAt >= from && a.CreatedAt <= to).Count();
                mktmodel.Feedback = db.Feedbacks.Where(a => a.CreatedAt >= from && a.CreatedAt <= to).Count();

                mktmodel.TotalRevenues = (db.OrderDetails.Where(a => a.CreatedAt >= from && a.CreatedAt <= to)).Sum(a => a.SalePrice);

                mktmodel.AverageStar = db.Feedbacks.Average(a => a.RateStar);
                mktmodel.NewMember = db.Accounts.OrderByDescending(a => a.Id).Where(a => a.IsActive == true && a.Role == 0 && a.CreatedAt >= from && a.CreatedAt <= to).Take(5);
                mktmodel.RevenuesCategories = (from s in db.OrderDetails // outer sequence
                                               join c in db.ProductCategories//inner sequence 
                                               on s.ProductId equals c.ProductId
                                               group c by c.CategoryId into r
                                               select new RevenuesOfCategory
                                               {
                                                   CategoryName = r.FirstOrDefault().Category.Title == null ? "Co the la null" : r.FirstOrDefault().Category.Title,
                                                   Revenue = r.Sum(s => s.Product.SalePrice)


                                               }).ToList();
                mktmodel.Trend = db.OrderDetails.GroupBy(o => o.ProductId).Select(group => new HotTrend
                { ProductName = group.FirstOrDefault().Product.Title, Count = group.Count() }).
                OrderByDescending(x => x.Count).FirstOrDefault();
            }
            else
            {

                mktmodel.NewAccount = db.Accounts.Count(a => a.IsActive == true);
                mktmodel.Posts = db.Sliders.Count();
                mktmodel.Product = db.Products.Count();
                mktmodel.Feedback = db.Feedbacks.Count();
                mktmodel.TotalRevenues = db.OrderDetails.Sum(t => t.SalePrice);
                mktmodel.AverageStar = db.Feedbacks.Average(a => a.RateStar);
                mktmodel.NewMember = db.Accounts.OrderByDescending(a => a.Id).Where(a => a.IsActive == true && a.Role == 0).Take(5);
                mktmodel.RevenuesCategories = (from s in db.OrderDetails // outer sequence
                                               join c in db.ProductCategories//inner sequence 
                                               on s.ProductId equals c.ProductId
                                               group c by c.CategoryId into r
                                               select new RevenuesOfCategory
                                               {
                                                   CategoryId = r.Key,
                                                   CategoryName = r.FirstOrDefault().Category.Title == null ? "Co the la null" : r.FirstOrDefault().Category.Title,
                                                   Revenue = r.Sum(s => s.Product.SalePrice)


                                               }).ToList();
                mktmodel.Trend = db.OrderDetails.GroupBy(o => o.ProductId).Select(group => new HotTrend
                { ProductName = group.FirstOrDefault().Product.Title, Count = group.Count() }).
                OrderByDescending(x => x.Count).FirstOrDefault();
            }

            return View(mktmodel);
        }
        [AuthenticationFilter]
        [AuthorizationFilter(role: "marketing")]
        public ActionResult PostDetail(int? id)
        {
            MKTPost model = new MKTPost();
            model.Title = "Post Detail";
            model.Tab = TabEnumMkt.Post;
            model.PostDetail = db.Blogs.Join(db.Accounts, (b => b.AuthorId), (a => a.Id), (b, a) => new PostViewModelz

            {

                Id = b.Id,

                AuthorName = a.FullName,

                Title = b.Title,

                Image = b.Image,

                Detail = b.Detail,

                CountView = b.CountView

            }).Where(p => p.Id == id).First();

            return View(model);
        }
        [HttpGet]
        public ActionResult PostEdit(int? id)
        {
            MKTPost model = new MKTPost();
            model.Title = "Post Edit";
            model.Tab = TabEnumMkt.Post;
            model.PostDetail = db.Blogs.Join(db.Accounts, (b => b.AuthorId), (a => a.Id), (b, a) => new PostViewModelz
            {

                Id = b.Id,

                AuthorName = a.FullName,

                Title = b.Title,

                Image = b.Image,

                Detail = b.Detail,

                CountView = b.CountView

            }).Where(p => p.Id == id).First();
            return View(model);
        }

        /*[AuthenticationFilter]
        [AuthorizationFilter(role: "marketing")]*/
        public ActionResult CustomerDetail(int? id)
        {
            MKTCustomer customer = new MKTCustomer();
            customer.Title = "Customer Detail";
            customer.Tab = TabEnumMkt.Customer;
            customer.Customer = db.Accounts.Where(c => c.Id == id).FirstOrDefault();
            return View(customer);
        }
        [HttpGet]
        public ActionResult CustomerEdit(int? id)
        {
            MKTCustomer customer = new MKTCustomer();
            customer.Title = "Customer Edit";
            customer.Tab = TabEnumMkt.Customer;
            customer.Customer = db.Accounts.Where(c => c.Id == id).FirstOrDefault();
            return View(customer);
        }

        [HttpPost]
        public ActionResult CustomerEdit(int? id, bool status)
        {
            MKTCustomer customer = new MKTCustomer();
            var edit = db.Accounts.Where(c => c.Id == id).FirstOrDefault();
            edit.IsActive = status;
            db.SaveChanges();
            return RedirectToAction("CustomerDetail","MKT", new {id=id});
        }

        // [AuthenticationFilter]
        //[AuthorizationFilter(role: "marketing")]
        public ActionResult FeedbackDetail(int? id)
        {
            MKTFeedbackDetail feedbackDetail = new MKTFeedbackDetail();
            feedbackDetail.Title = "Feedback Detail";
            feedbackDetail.Tab = TabEnumMkt.Feedback;
            feedbackDetail.Feedback = db.Feedbacks.Where(f => f.Id == id).FirstOrDefault();
            var orderdetail = db.OrderDetails.Where(od => od.FeedbackId == feedbackDetail.Feedback.Id).First();
            var order = db.Orders.Where(o => o.Id == orderdetail.OrderId).First();
            feedbackDetail.CustomerFeedback = db.Accounts.Where(a => a.Id == order.AccountId).First();
            feedbackDetail.FeedbackProductImage = db.Products.Where(p => p.Id == orderdetail.ProductId).First();
            return View(feedbackDetail);
        }
        [HttpPost]
        public ActionResult PostEdit(int? id, HttpPostedFileBase image,
            string title,
            string detail)
        {
            MKTPost model = new MKTPost();
            var editpost = db.Blogs.Where(p => p.Id == id).First();
            if (image != null)
            {
                // save image to temp folder
                string newFileName = DateTime.Now.Ticks + "_" + image.FileName;
                string path = Path.Combine(Server.MapPath("~/TempImageUpload"), newFileName);
                image.SaveAs(path);
                // upload to cloudinary and get url of image
                string[] url = CloudinaryConfig.Upload(new string[] { path });
                // delete image from temp folder
                System.IO.File.Delete(path);
                editpost.Image = url[0];
            }
            editpost.Title = title;
            editpost.Detail = detail;
            db.SaveChanges();
            return RedirectToAction("PostList", "MKT");
        }
        public ActionResult DeletePostz(int id)
        {
            var deletepost = db.Blogs.Where(b => b.Id == id).First();
            db.Blogs.Remove(deletepost);
            db.SaveChanges();
            return RedirectToAction("PostList", "MKT");
        }

        // GET: Post
        /*[AuthenticationFilter]
        [AuthorizationFilter("marketing")]*/
        public ActionResult PostList(int? authorId, string idOrName, int? id)
        {
            PostListViewModelz plvmodel = new PostListViewModelz();
            plvmodel.Title = "Post List";
            plvmodel.Tab = TabEnumMkt.Post;
            if (String.IsNullOrEmpty(idOrName))
            { plvmodel.Title = "Post List";
            plvmodel.Tab = TabEnumMkt.Post;
                idOrName = "";
            }
            try
            {
                int iD = Int32.Parse(idOrName);
                if (id == null && authorId == null)
                {
                    var authorName = db.Blogs.Join(db.Accounts, (b => b.AuthorId), (a => a.Id), (b, a) => new PostViewModelz
                    {
                        Id = b.Id,
                        DateCreate = b.CreatedAt,
                        DateUpdate = b.UpdatedAt,
                        AuthorName = a.FullName,
                        Title = b.Title,
                        Image = b.Image,
                        Detail = b.Detail,
                        CountView = b.CountView
                    }).Where(o=>o.Id.ToString().Contains(idOrName)).Take(7).ToList();
                    plvmodel.Posts = authorName;
                    ViewBag.count = db.Blogs.Where(o => o.Id.ToString().Contains(idOrName)).Count();

                }
                if (id != null && authorId != null)
                {
                    var authorName = db.Blogs.Where(o => o.Id.ToString().Contains(idOrName) && o.AuthorId == authorId).Join(db.Accounts, (b => b.AuthorId), (a => a.Id), (b, a) => new PostViewModelz
                    {
                        Id = b.Id,
                        AuthorName = a.FullName,
                        DateCreate = b.CreatedAt,
                        DateUpdate = b.UpdatedAt,
                        Title = b.Title,
                        Image = b.Image,
                        Detail = b.Detail,
                        CountView = b.CountView
                    }).OrderBy(i => 0).Skip((id.GetValueOrDefault() - 1) * 7).Take(7).ToList();
                    plvmodel.Posts = authorName;
                    ViewBag.count = db.Blogs.Where(o => o.Id.ToString().Contains(idOrName) && o.AuthorId == authorId).Count();
                }
                if (id == null && authorId != null)
                {
                    var authorName = db.Blogs.Where(o => o.Id.ToString().Contains(idOrName) && o.AuthorId == authorId).Join(db.Accounts, (b => b.AuthorId), (a => a.Id), (b, a) => new PostViewModelz
                    {
                        Id = b.Id,
                        AuthorName = a.FullName,
                        Title = b.Title,
                        DateCreate = b.CreatedAt,
                        DateUpdate = b.UpdatedAt,
                        Image = b.Image,
                        Detail = b.Detail,
                        CountView = b.CountView
                    }).Where(o => o.Id.ToString().Contains(idOrName)).Take(7).ToList();
                    plvmodel.Posts = authorName;
                    ViewBag.count = db.Blogs.Where(o => o.Id.ToString().Contains(idOrName) && o.AuthorId == authorId).Count();
                }
                if (id != null && authorId == null)
                {
                    var authorName = db.Blogs.Where(o => o.Id.ToString().Contains(idOrName)).Join(db.Accounts, (b => b.AuthorId), (a => a.Id), (b, a) => new PostViewModelz
                    {
                        Id = b.Id,
                        AuthorName = a.FullName,
                        DateCreate = b.CreatedAt,
                        DateUpdate = b.UpdatedAt,
                        Title = b.Title,
                        Image = b.Image,
                        Detail = b.Detail,
                        CountView = b.CountView
                    }).OrderBy(i => 0).Skip((id.GetValueOrDefault() - 1) * 7).Take(7).ToList();
                    plvmodel.Posts = authorName;
                    ViewBag.count = db.Blogs.Where(o => o.Id.ToString().Contains(idOrName)).Count();
                }

                ViewBag.count = db.Blogs.Where(o => o.Id.ToString().Contains(idOrName)).Count();
            }
            catch (Exception) {
                if (id == null && authorId == null)
                {
                    var authorName = db.Blogs.Join(db.Accounts, (b => b.AuthorId), (a => a.Id), (b, a) => new PostViewModelz
                    {
                        Id = b.Id,
                        AuthorName = a.FullName,
                        DateCreate = b.CreatedAt,
                        DateUpdate = b.UpdatedAt,
                        Title = b.Title,
                        Image = b.Image,
                        Detail = b.Detail,
                        CountView = b.CountView
                    }).Where(o => o.Title.Contains(idOrName)).Take(7).ToList();
                    plvmodel.Posts = authorName;
                    ViewBag.count = db.Blogs.Where(o => o.Title.Contains(idOrName)).Count();
                }
                if (id != null && authorId != null)
                {
                    var authorName = db.Blogs.Where(o => o.Title.Contains(idOrName) && o.AuthorId == authorId).Join(db.Accounts, (b => b.AuthorId), (a => a.Id), (b, a) => new PostViewModelz
                    {
                        Id = b.Id,
                        AuthorName = a.FullName,
                        DateCreate = b.CreatedAt,
                        DateUpdate = b.UpdatedAt,
                        Title = b.Title,
                        Image = b.Image,
                        Detail = b.Detail,
                        CountView = b.CountView
                    }).OrderBy(i => 0).Skip((id.GetValueOrDefault() - 1) * 7).Take(7).ToList();
                    plvmodel.Posts = authorName;
                    ViewBag.count = db.Blogs.Where(o => o.Title.Contains(idOrName) && o.AuthorId == authorId).Count();
                }
                if (id == null && authorId != null)
                {
                    var authorName = db.Blogs.Where(o => o.Title.Contains(idOrName) && o.AuthorId == authorId).Join(db.Accounts, (b => b.AuthorId), (a => a.Id), (b, a) => new PostViewModelz
                    {
                        Id = b.Id,
                        AuthorName = a.FullName,
                        DateCreate = b.CreatedAt,
                        DateUpdate = b.UpdatedAt,
                        Title = b.Title,
                        Image = b.Image,
                        Detail = b.Detail,
                        CountView = b.CountView
                    }).Where(o => o.Id.ToString().Contains(idOrName)).Take(7).ToList();
                    plvmodel.Posts = authorName;
                    ViewBag.count = db.Blogs.Where(o => o.Title.Contains(idOrName) && o.AuthorId == authorId).Count();
                }
                if (id != null && authorId == null)
                {
                    var authorName = db.Blogs.Where(o => o.Title.Contains(idOrName)).Join(db.Accounts, (b => b.AuthorId), (a => a.Id), (b, a) => new PostViewModelz
                    {
                        Id = b.Id,
                        AuthorName = a.FullName,
                        DateCreate = b.CreatedAt,
                        DateUpdate = b.UpdatedAt,
                        Title = b.Title,
                        Image = b.Image,
                        Detail = b.Detail,
                        CountView = b.CountView
                    }).OrderBy(i => 0).Skip((id.GetValueOrDefault() - 1) * 7).Take(7).ToList();
                    plvmodel.Posts = authorName;
                    ViewBag.count = db.Blogs.Where(o => o.Title.Contains(idOrName)).Count();
                }
                

            }
           
            plvmodel.Authors = db.Accounts.ToList();
            if (id == null || id == 1)
            {
                ViewBag.Index = 1;
            }
            else 
            { ViewBag.Index = id; }
            ViewBag.AuthorId = authorId;
            ViewBag.idOrName = idOrName;
            /*var author = db.Accounts.Where(o => o.Id == authorId).FirstOrDefault();
            if(author != null)
            {
                ViewBag.AuthorName = author.FullName;
            }*/
            return View(plvmodel);
        }
        [HttpGet]
        [Route("MKT/DeletePost/{id:int}/{index:int}")]

        public ActionResult DeletePost(int id, int index)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            else
            {
                var blog = db.Blogs.Where(o => o.Id == id).FirstOrDefault();
                if (blog == null)
                {
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
                }
                else
                {
                    db.Blogs.Attach(blog);
                    db.Blogs.Remove(blog);
                    db.SaveChanges();
                }
                return RedirectToAction("PostList", "MKT", new { id = index });
            }
        }
        
        [HttpPost]
        public ActionResult SearchPost(FormCollection form)
        {
            var idOrName = HttpContext.Request.Form["idOrName"].Trim();
            var authorId = HttpContext.Request.Form["author"];          
            return RedirectToAction("PostList", new {id = 1,idOrName = idOrName,authorId = authorId});

        }

        

        [HttpPost]
        public ActionResult CreatePost(FormCollection form)
        {
            Blog blog = new Blog();
            blog.Title = HttpContext.Request.Form["Title"];
            blog.Detail = HttpContext.Request.Form["Detail"];
            blog.AuthorId = Int32.Parse(HttpContext.Request.Form["Author"]);
            var filebase = (HttpPostedFileBase)HttpContext.Request.Files["File"];
            if (filebase != null)
            {
                // save image to temp folder
                string newFileName = DateTime.Now.Ticks + "_" + filebase.FileName;
                string path = Path.Combine(Server.MapPath("~/TempImageUpload"), newFileName);
                filebase.SaveAs(path);

                // upload to cloudinary and get url of image
                string[] url = CloudinaryConfig.Upload(new string[] { path });

                // delete image from temp folder
                System.IO.File.Delete(path);

                blog.Image = url[0];
            }
            blog.CreatedAt = DateTime.Now.Date;
            blog.UpdatedAt = DateTime.Now.Date;
            db.Blogs.Add(blog);
            db.SaveChanges();
            TempData["successmessage"] = "Create post successfully";
            return RedirectToAction("PostList", "MKT");
        }

        public ActionResult ProductList(int? index, int? categoryId, string idOrName)
        {
            MKTProductDetailList mktpdl = new MKTProductDetailList();
            mktpdl.Title = "Product List";
            mktpdl.Tab = TabEnumMkt.Product;
            List<int> listProductId = new List<int>();
            List<Product>listProduct = new List<Product>();
            List<MKTProductDetailz> listMTKPD = new List<MKTProductDetailz>();
            List<string> categories = new List<string>();
            ViewBag.categoryId = categoryId;
            ViewBag.idOrName = idOrName;
            if (Session["listCategory"] == null)
            {
                Session["listCategory"] = db.Categories.ToList();
            }           
            if (String.IsNullOrEmpty(idOrName))
            {
                idOrName = "";
            }           
            try
            {
                int id = Int32.Parse(idOrName);
                if(index == null || index == 1)
                {
                    ViewBag.Index = 1;
                    if(categoryId == null)
                    {
                        ViewBag.CountList = db.Products.Where(o => o.Id.ToString().Contains(idOrName)).Count();
                        listProduct = db.Products.Where(o => o.Id.ToString().Contains(idOrName)).Take(9).ToList();
                        listProduct.ForEach(v => listMTKPD.Add(new MKTProductDetailz(v, ListImage(v),ListCategory(v.Id))));
                    }
                    else
                    {
                        ViewBag.CountList = db.ProductCategories.Where(o => o.CategoryId == categoryId && o.ProductId.ToString().Contains(idOrName)).Count();
                        listProductId = db.ProductCategories.Where(o => o.CategoryId == categoryId).Select(o => o.ProductId).Take(9).ToList();
                        /*listProductId.ForEach(v => listProduct.Add(db.Products.Where(o => o.Id == v && o.Id.ToString().Contains(idOrName)).FirstOrDefault()));*/
                        foreach(var item in listProductId)
                        {
                            var product = db.Products.Where(o => o.Id == item && o.Id.ToString().Contains(idOrName)).FirstOrDefault();
                            if(product == null)
                            {
                                continue;
                            }
                            else
                            {
                                listProduct.Add(product);
                            }
                        }
                        listProduct.ForEach(v => listMTKPD.Add(new MKTProductDetailz(v, ListImage(v), ListCategory(v.Id))));
                    }
                    mktpdl.MKTProductDetails = listMTKPD;
                    return View(mktpdl);
                }
                else
                {
                    ViewBag.Index = index;
                    if (categoryId == null)
                    {
                        ViewBag.CountList = db.Products.Where(o => o.Id.ToString().Contains(idOrName)).Count();
                        listProduct = db.Products.Where(o => o.Id.ToString().Contains(idOrName)).OrderBy(o=>0).Skip((index.GetValueOrDefault() - 1) * 9).Take(9).ToList();
                        listProduct.ForEach(v => listMTKPD.Add(new MKTProductDetailz(v, ListImage(v), ListCategory(v.Id))));
                    }
                    else
                    {
                        ViewBag.CountList = db.ProductCategories.Where(o => o.CategoryId == categoryId && o.ProductId.ToString().Contains(idOrName)).Count();
                        listProductId = db.ProductCategories.Where(o => o.CategoryId == categoryId).Select(o => o.ProductId).OrderBy(o => 0).Skip((index.GetValueOrDefault() - 1) * 9).Take(9).ToList();
                        /*listProductId.ForEach(v => listProduct.Add(db.Products.Where(o => o.Id == v && o.Id.ToString().Contains(idOrName)).FirstOrDefault()));*/
                        foreach (var item in listProductId)
                        {
                            var product = db.Products.Where(o => o.Id == item && o.Id.ToString().Contains(idOrName)).FirstOrDefault();
                            if (product == null)
                            {
                                continue;
                            }
                            else
                            {
                                listProduct.Add(product);
                            }
                        }
                        listProduct.ForEach(v => listMTKPD.Add(new MKTProductDetailz(v, ListImage(v), ListCategory(v.Id))));
                    }
                    mktpdl.MKTProductDetails = listMTKPD;
                    return View(mktpdl);
                }
                
            }
            catch (Exception)
            {
                if (index == null || index == 1)
                {
                    ViewBag.Index = 1;
                    if (categoryId == null)
                    {
                        ViewBag.CountList = db.Products.Where(o => o.Title.ToString().Contains(idOrName)).Count();
                        listProduct = db.Products.Where(o => o.Title.ToString().Contains(idOrName)).Take(9).ToList();
                        listProduct.ForEach(v => listMTKPD.Add(new MKTProductDetailz(v, ListImage(v), ListCategory(v.Id))));
                    }
                    else
                    {
                        var listProductIdFull = db.ProductCategories.Where(o => o.CategoryId == categoryId).Select(o => o.ProductId).ToList();
                        ViewBag.CountList = ListProduct(listProductIdFull).Where(o => o.Title.Contains(idOrName)).Count();
                        listProductId = db.ProductCategories.Where(o => o.CategoryId == categoryId).Select(o => o.ProductId).Take(9).ToList();
                        /*listProductId.ForEach(v => listProduct.Add(db.Products.Where(o => o.Id == v && o.Title.ToString().Contains(idOrName)).FirstOrDefault()));*/
                        foreach (var item in listProductId)
                        {
                            var product = db.Products.Where(o => o.Id == item && o.Title.ToString().Contains(idOrName)).FirstOrDefault();
                            if (product == null)
                            {
                                continue;
                            }
                            else
                            {
                                listProduct.Add(product);
                            }
                        }
                        listProduct.ForEach(v => listMTKPD.Add(new MKTProductDetailz(v==null?new Product():v, ListImage(v), ListCategory(v.Id))));
                    }
                    mktpdl.MKTProductDetails = listMTKPD;
                    return View(mktpdl);

                }
                else
                {
                    ViewBag.Index = index;
                    if (categoryId == null)
                    {
                        ViewBag.CountList = db.Products.Where(o => o.Title.ToString().Contains(idOrName)).Count();
                        listProduct = db.Products.Where(o => o.Title.ToString().Contains(idOrName)).OrderBy(o => 0).Skip((index.GetValueOrDefault() - 1) * 9).Take(9).ToList();
                        listProduct.ForEach(v => listMTKPD.Add(new MKTProductDetailz(v, ListImage(v), ListCategory(v.Id))));
                    }
                    else
                    {
                        var listProductIdFull = db.ProductCategories.Where(o => o.CategoryId == categoryId).Select(o => o.ProductId).ToList();
                        ViewBag.CountList = ListProduct(listProductIdFull).Where(o => o.Title.Contains(idOrName)).Count();
                        listProductId = db.ProductCategories.Where(o => o.CategoryId == categoryId).Select(o => o.ProductId).OrderBy(o => 0).Skip((index.GetValueOrDefault() - 1) * 9).Take(9).ToList();
                        /* listProductId.ForEach(v => listProduct.Add(db.Products.Where(o => o.Id == v && o.Title.ToString().Contains(idOrName)).FirstOrDefault()));*/
                        foreach (var item in listProductId)
                        {
                            var product = db.Products.Where(o => o.Id == item && o.Title.ToString().Contains(idOrName)).FirstOrDefault();
                            if (product == null)
                            {
                                continue;
                            }
                            else
                            {
                                listProduct.Add(product);
                            }
                        }
                        listProduct.ForEach(v => listMTKPD.Add(new MKTProductDetailz(v, ListImage(v), ListCategory(v.Id))));
                    }
                    mktpdl.MKTProductDetails = listMTKPD;
                    return View(mktpdl);
                }
            }
            
        }

        public List<string> ListImage(Product p)
        {
            if(p == null)
            {
                return new List<string>();
            }
            var x = db.ImageProducts.Where(o => o.ProductId == p.Id);
            if(x == null)
            {
                return new List<string>();
            }
            else
            {
                return x.Select(o => o.Image).ToList();
            }
            
        }
        public List<Product> ListProduct(List<int> productId)
        {
            List<Product>listProduct = new List<Product>();
            productId.ForEach(v => listProduct.Add(db.Products.Where(o => o.Id == v).FirstOrDefault()));
            return listProduct;
        }
        public List<Category> ListCategory(int productId)
        {
            List<Category> listCategories = new List<Category>();
            var listCateGoriesId = db.ProductCategories.Where(o => o.ProductId == productId).Select(o=>o.CategoryId).ToList();
            listCateGoriesId.ForEach(v => listCategories.Add(db.Categories.Where(o => o.Id == v).FirstOrDefault()));
            return listCategories;
        }
        [HttpPost]
        public ActionResult SearchProduct(FormCollection form)
        {
            var idOrName = HttpContext.Request.Form["idOrName"].Trim();
            var categoryId = HttpContext.Request.Form["category"];
            return RedirectToAction("ProductList", new { id = 1, idOrName = idOrName, categoryId = categoryId });
        }
       

        [HttpGet]
        [Route("MKT/DeleteProduct/{id:int}/{index:int}")]

        public ActionResult DeleteProduct(int id, int index)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            else
            {
                var product = db.Products.Where(o => o.Id == id).FirstOrDefault();
                var imageProduct = db.ImageProducts.Where(o => o.ProductId == id).ToList();
                if (product == null || imageProduct == null || imageProduct.Count==0)
                {
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
                }
                else
                {
                    db.Products.Attach(product);
                    db.Products.Remove(product);
                    imageProduct.ForEach(v => { db.ImageProducts.Attach(v); 
                                                db.ImageProducts.Remove(v); });
                    db.SaveChanges();
                }
                return RedirectToAction("ProductList", "MKT", new { index = index });
            }
        }

        [HttpPost]
        public ActionResult CreateProduct(FormCollection form)
        {
            Product p = new Product();
            p.Title= HttpContext.Request.Form["Title"];
            p.Detail = HttpContext.Request.Form["Detail"];
            p.OriginalPrice =Int32.Parse(HttpContext.Request.Form["price"]);
            p.SalePrice = Int32.Parse(HttpContext.Request.Form["price"]);
            p.CreatedAt = DateTime.Now;
            p.UpdatedAt = DateTime.Now;
            db.Products.Add(p);
            db.SaveChanges();
           
            HttpFileCollectionBase files = HttpContext.Request.Files;
            foreach(string key in files)
            {
                var file = files[key];
                if (file != null)
                {
                    ImageProduct ip = new ImageProduct();

                    string newFileName = DateTime.Now.Ticks + "_" + file.FileName;
                    string path = Path.Combine(Server.MapPath("~/TempImageUpload"), newFileName);
                    file.SaveAs(path);
                    string[] url = CloudinaryConfig.Upload(new string[] { path });

                    // delete image from temp folder
                    System.IO.File.Delete(path);

                    ip.Image = url[0];
                    ip.CreatedAt = DateTime.Now;
                    ip.UpdatedAt = DateTime.Now;
                    ip.ProductId = p.Id;
                    db.ImageProducts.Add(ip);
                    db.SaveChanges();

                }
            }

            ProductCategory pc = new ProductCategory();
            pc.ProductId = p.Id;
            pc.CategoryId = Int32.Parse(HttpContext.Request.Form["category"]);
            pc.CreatedAt = DateTime.Now;
            pc.UpdatedAt = DateTime.Now;
            db.ProductCategories.Add(pc);
            db.SaveChanges();           
            TempData["successmessage"] = "Create product successfully";
            return RedirectToAction("ProductList", "MKT");
        }
        [HttpGet]
        public ActionResult ProductDetail(int? id)
        {

            MKTProductDetail mktModel = new MKTProductDetail();
            mktModel.Title = "Product Detail";
            mktModel.Tab = TabEnumMkt.Product;
            mktModel.ProductCategory = db.ProductCategories.Where(c => c.ProductId == id);
            mktModel.ImageProducts = db.ImageProducts.Where(i => i.ProductId == id);
           
            return View(mktModel);
        }

        [HttpGet]
        public ActionResult ProductEdit(int? id)
        {
            MKTProductDetail mktModel = new MKTProductDetail();
            mktModel.Title = "Product Edit";
            mktModel.Tab = TabEnumMkt.Product;
            mktModel.ProductCategory = db.ProductCategories.Where(c => c.ProductId == id);
            mktModel.ImageProducts = db.ImageProducts.Where(i => i.ProductId == id);
            Session["ProId"] = mktModel.ProductCategory.FirstOrDefault().ProductId;
            return View(mktModel);
        }
        [HttpPost]
        public ActionResult ProductEdit(int? id, string title, string detail, int sale, int original )
        {
            MKTProductDetail mktModel = new MKTProductDetail();
            mktModel.Title = "Product Edit";
            mktModel.Tab = TabEnumMkt.Product;
            var cate = db.ProductCategories.Where(c => c.ProductId == id);
           // var imageProducts = db.ImageProducts.Where(i => i.ProductId == id && i.Id==imgID).FirstOrDefault();
           
            var product = db.Products.Where(p => p.Id == id).First();
            product.Title = title;
            product.Detail = detail;
            product.SalePrice = sale;
            product.OriginalPrice = original;
            
            db.SaveChanges();
            return RedirectToAction("ProductList", "MKT");
        }
        [HttpPost]
        public ActionResult DeleteImgProduct(int id)
        {
            var delete = db.ImageProducts.Where(b => b.Id == id).First();
            db.ImageProducts.Remove(delete);
            db.SaveChanges();
            return null;
        }
        //[HttpPost]
        public ActionResult AddImg(HttpPostedFileBase image)
        {
            MKTProductDetail mktModel = new MKTProductDetail();
            mktModel.Title = "Product Detail";
            mktModel.Tab = TabEnumMkt.Product;
            var proId = Int32.Parse(Session["ProId"].ToString());
            ImageProduct img = new ImageProduct();
            
                if (image != null)
            {
                // save image to temp folder
                string newFileName = DateTime.Now.Ticks + "_" + image.FileName;
                string path = Path.Combine(Server.MapPath("~/TempImageUpload"), newFileName);
                image.SaveAs(path);
                // upload to cloudinary and get url of image
                string[] url = CloudinaryConfig.Upload(new string[] { path });
                // delete image from temp folder
                System.IO.File.Delete(path);
                img.Image = url[0];
            }
                img.ProductId = proId;
            img.CreatedAt = DateTime.Now;
            img.UpdatedAt =DateTime.Now;
            
            db.ImageProducts.Add(img);
            db.SaveChanges();
            return RedirectToAction("ProductEdit","MKT",new {id = proId });
            

            
        }
    }



}

