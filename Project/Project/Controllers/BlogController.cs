﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Web.Mvc;
using Project.Models;
using System.Linq;
using Project.Filter;


namespace Project.Controllers
{
    
    public class BlogController : Controller
    {
        private ProjectEntities db = new ProjectEntities();
        // GET: Blog
        /*[AuthenticationFilter]
        [AuthorizationFilter("customer")]*/
        public ActionResult Index(int? id)
        {
            BlogListViewModel viewModelBlog = new BlogListViewModel();
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).FirstOrDefault();
                viewModelBlog.Account = selectAccount;
            }
            if (id == null || id == 1)
            {
                ViewBag.Index = 1;
            }
            else
            { ViewBag.Index = id; }

            if (id == null)
            {
               
                var lb = db.Blogs.Take(9).ToList();
                if (lb == null)
                {
                    return new EmptyResult();
                }
                viewModelBlog.CountList = db.Blogs.ToList().Count();
                viewModelBlog.BlogList = lb;
                return View(viewModelBlog);
            }
            else {               
                    var lb = (from e in db.Blogs select e).OrderBy(i => 0).Skip((id.GetValueOrDefault() - 1) * 9).Take(9).ToList();
                    if (lb == null)
                    {
                        return new EmptyResult();
                    }
                    viewModelBlog.CountList = db.Blogs.Count();
                    viewModelBlog.BlogList = lb;
                    
                    return View(viewModelBlog);
            } 
        }
        public ActionResult BlogDetail(int? id)
        {
            Blog a = new Blog();
          
            BlogListViewModel blm = new BlogListViewModel();
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                blm.Account = selectAccount;
                
                
            }
            //get detail post
            var detail = (from x in db.Blogs where x.Id == id select x).First();


            //update countview
            detail.CountView = detail.CountView + 1;
            db.SaveChanges();

            a.Id = detail.Id;
            a.Title = detail.Title;
            a.AuthorId = detail.AuthorId;
            a.CreatedAt = detail.CreatedAt;
            a.UpdatedAt = detail.UpdatedAt;
            a.Detail = detail.Detail;
            a.Image = detail.Image;
            a.CountView = detail.CountView;
            blm.blog = a;
            

            //get author's post infor
            int author_id = detail.AuthorId;
            var author = (from x in db.Accounts where x.Id == author_id select x).First();
            Account au = new Account();
            au.Id = author.Id;
            au.FullName = author.FullName;
            au.Image = author.Image;
            blm.Author = au;

            //get 3 lastest post
            Dictionary < Blog, Account > last_post = new Dictionary<Blog, Account>();
            var lastest_post = db.Blogs.Take(3).OrderByDescending(x => x.Id).ToList();
            if (lastest_post != null)
            {
                foreach(var item in lastest_post)
                {
                    int au_id = item.AuthorId;
                    Account aut = db.Accounts.Where(x => x.Id == au_id).First();
                    last_post.Add(item,aut);
                } 
            }
            blm.LastestPost = last_post;

            //get trending post
            Dictionary<Blog, Account> trending = new Dictionary<Blog, Account>();
            var trending_post = db.Blogs.Take(2).OrderByDescending(x => x.CountView).ToList();
            if(trending_post != null)
            {
                foreach(var item in trending_post)
                {
                    int au_id_trend = item.AuthorId;
                    Account aut = db.Accounts.Where(x => x.Id == au_id_trend).First();
                    trending.Add(item, aut);
                }
            }
            blm.TrendingPost = trending;

            return View(blm);
        }

    }
}