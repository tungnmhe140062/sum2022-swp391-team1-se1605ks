﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.Filter;
using Project.Models;
using Project.Models.manage;
namespace Project.Controllers
{
    public class OrderController : Controller
    {        
        private ProjectEntities db = new ProjectEntities();
        SaleCommonViewModel ovm = new SaleCommonViewModel();
        public int pageSize = 5;
        // GET: Order

        [AuthenticationFilter]
        [AuthorizationFilter(role: "sale;sale manager")]
        public ActionResult Index(int? page, int? sortby, string filter)
        {
            ovm.Title = "OrderList";
            ovm.Tab = TabEnumSale.OrderList;
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                ovm.Account = selectAccount;
            }
            Dictionary<Order, int> list = null;
            string param = "";
            if(page == null)
            {
                page = 1;
                
            }
            ovm.current_page = (int)page;
            if (filter == null || filter == "")
            {
                list = OrderList((int)page, filter);
                ovm.CountOrder = (int)Math.Ceiling((double)CountOrder("") / (double)pageSize);
                ovm.filter = null;
            }
            else if(filter != "" || filter!=null)
            {
                ovm.filter = filter;
                list = OrderList((int)page, filter);
                ovm.CountOrder = (int)Math.Ceiling((double)CountOrder(filter) / pageSize);
                param += "&filter=" + filter;
            }
            
            ovm.SortBy = 0;
            //sort by
            if (sortby != null && list !=null)
            {
                ovm.SortBy = (int)sortby;
                switch ((int)sortby)
                {
                    //date
                    case 1: list = list.OrderBy (x => x.Key.CreatedAt).ToDictionary(obj => obj.Key, obj => obj.Value);
                            param += "&sortby=" + (int)sortby;
                            break;
                    //fullname
                    case 2: list = list.OrderBy(x => x.Key.FullName).ToDictionary(obj => obj.Key, obj => obj.Value);
                            param += "&sortby=" + (int)sortby;
                            break;
                    //total price
                    case 3: list = list.OrderByDescending(x => x.Value).ToDictionary(obj => obj.Key, obj => obj.Value); 
                            param += "&sortby=" + (int)sortby;
                            break;
                }
                ovm.OrderList = list;
            }
            else
            {
                ovm.OrderList = list;
            }
            ovm.param = param;
            return View(ovm);
        }

        //countorder

        // order's status { 0: order placed, 1: processing, 2:shipping, 3: cancel, 4: complete }
        public int CountOrder(string param)
        {
            int result = 0;
            switch (param)
            {
                case "":
                    result = db.Orders.Count();
                    break;
                case "placed":
                    result = db.Orders.Where(x => x.Status == 0).Count();
                    break;
                case "processing":
                    result = db.Orders.Where(x => x.Status == 1).Count();
                    break;
                case "shipping":
                    result = db.Orders.Where(x => x.Status == 2).Count();
                    break;
                case "cancel":
                    result = db.Orders.Where(x => x.Status == 3).Count();
                    break;
                case "complete":
                    result = db.Orders.Where(x => x.Status == 4).Count();
                    break;
            } 
            return result;
        }
        public int GetOrderStatus(string param)
        {
            int result = 0;
            switch (param)
            {
                case "placed":
                    result = 0;
                    break;
                case "processing":
                    result = 1;
                    break;
                case "shipping":
                    result = 2;
                    break;
                case "cancel":
                    result = 3;
                    break;
                case "complete":
                    result = 4;
                    break;
            }
            return result;
        }

        public List<Order> GetOrderByFilter(string filter)
        {
            List<Order> list = null;
            int status_id = GetOrderStatus(filter);
            list = db.Orders.Where(x => x.Status == status_id).ToList();
            return list;
        }
        //orderlist
        public Dictionary<Order,int> OrderList(int page, string filter)
        {
            List<Order> o_list = null;
            
            Dictionary<Order, int> list = new Dictionary<Order, int>();
            //get orderlist if filter null
            
            if (filter == null)
            {
                o_list = db.Orders.OrderByDescending(x => x.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                o_list = GetOrderByFilter(filter).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            if (o_list != null)
            {
                foreach(var order in o_list)
                {
                    var detail_list = db.OrderDetails.Where(x => x.OrderId == order.Id).ToList();
                    int total_price = 0;
                    if(detail_list != null)
                    {
                        total_price = detail_list.Sum(x => x.SalePrice);
                    }
                    list.Add(order, total_price);
                }
            }
            return list;
        }

        public ActionResult Details(int id)
        {
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                ovm.Account = selectAccount;
            }
            ovm.Title = "OrderDetail";
            ovm.Tab = TabEnumSale.OrderDetail;

            Dictionary<OrderDetail, Product> list = new Dictionary<OrderDetail, Product>();
            //set list seller to assign
            var seller_list = db.Accounts.Where(x => x.Role == 1 && x.IsActive == true).ToList();
            ovm.seller_list = seller_list;
            
            var order_detail = db.OrderDetails.Where(x => x.OrderId == id).ToList();
            var order = db.Orders.Where(x => x.Id == id).FirstOrDefault();
            if(order_detail != null && order !=null)
            {
                Order o = (Order)order;
                
                //set seller name to view
                int seller_id = Convert.ToInt32(o.AssignedTo);
                Account seller_name = db.Accounts.Where(x => x.Id == seller_id).First();
                ovm.SellerName = seller_name.FullName;

                // set status name
                ovm.status_name = GetStatusName(o.Status);

                ovm.Order = o;
                foreach(var items in order_detail)
                {
                    var product = db.Products.Where(x => x.Id == items.ProductId).First();
                    list.Add(items, product);
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
            ovm.Order_detail = list;
            return View(ovm);
        }
        [HttpPost]
        public ActionResult Assign(int order_id, int status_id, int seller_id) 
        {
            var order = db.Orders.Where(x => x.Id == order_id).FirstOrDefault();
            order.Status = status_id;
            order.AssignedTo = seller_id;
            db.SaveChanges();
            return RedirectToAction("Details","Order",new {id = order_id});
        }

        public string GetStatusName(int status_id)
        {
            string result = "";
            switch (status_id)
            {
                case 0: result = "Order Placed";break;
                case 1: result = "Processing";break;
                case 2: result = "Shipping";break;
                case 3: result = "Canceled";break;
                case 4: result = "Completed";break;
            }
            return result;
        }

    }
}