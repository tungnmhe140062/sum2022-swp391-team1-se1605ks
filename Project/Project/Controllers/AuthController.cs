using System;
using System.Data.Entity.Core;
using System.Data.Entity.Validation;
using System.Web.Mvc;
using Project.Filter;
using Project.Models;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Net.Mail;
using System.Net;
using WebGrease.Activities;

namespace Project.Controllers
{
    public class AuthController : Controller
    {
        private ProjectEntities db = new ProjectEntities();

        [HttpPost]
        public ActionResult Login(string email, string password)
        {
            Console.Out.WriteLine(email);
            Console.Out.WriteLine(password);

            var searchAccount =
                (from e in db.Accounts where e.Email == email && e.Password == password select e).ToList();
            if (searchAccount.Count == 0)
            {
                TempData["isLoginFail"] = true;              
            }
            else if (searchAccount[0].IsActive == false)
            {
                TempData["isNotActiveAccount"] = true;
            }
            else    
            { 
                Session["userId"] = searchAccount[0].Id;
                switch (searchAccount[0].Role)
                {
                    case 0:
                        return RedirectToAction("Index", "Home");
                    case 1:
                        return RedirectToAction("Index", "Sale");
                    case 2:
                        return RedirectToAction("Index", "Sale");
                    case 3:
                        return RedirectToAction("Dashboard", "MKT");
                    case 4:
                        return RedirectToAction("Dashboard", "Admin");
                }
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Register(
            string name,
            string gender,
            string mobile,
            string address,
            string email,
            string password)
        {
            // generate random string
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random random = new Random();
            
            string code = new string(Enumerable.Repeat(chars, 32)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            
            // check regex password
            bool isValidPassword = Regex.IsMatch(password, @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$");
            if (!isValidPassword)
            {
                TempData["isInvalidPasswordRegister"] = true;
                return RedirectToAction("Index", "Home");
            }
            
            // check phone number
            bool isValidPhone = Regex.IsMatch(mobile, @"^[0-9]{10}$");
            if (!isValidPhone)
            {
                TempData["isInvalidPhoneRegister"] = true;
                return RedirectToAction("Index", "Home");
            }
            
            Account account = new Account()
            {
                FullName = name,
                Gender = Int32.Parse(gender),
                Phone = mobile,
                Address = address,
                Email = email,
                Password = password,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                CodeActive = code
            };
            
            try
            {
                db.Accounts.Add(account);
                db.SaveChanges();
            
                TempData["isRegisterSuccess"] = true;
            
                SendGridConfig.SendEmailActive(email, code);
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                TempData["isRegisterFail"] = true;
            }
            
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Active(string code)
        {
            var selectAccount = (from e in db.Accounts where e.IsActive == false && e.CodeActive == code select e)
                .ToList();

            if (selectAccount.Count == 0)
            {
                TempData["activeFail"] = true;
            }
            else
            {
                selectAccount[0].IsActive = true;
                selectAccount[0].CodeActive = null;
            }

            try
            {
                db.SaveChanges();
                TempData["activeSuccess"] = true;
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message);
                TempData["activeFail"] = true;
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult ChangePassword(string oldPass, string newPass, string rePass)
        {
            int id = Convert.ToInt32(Session["userId"].ToString());           
            try
            {
                var update = db.Accounts.First(g => g.Id == id);
                if (newPass.Equals(rePass) && oldPass.Equals(update.Password) && !newPass.Equals(oldPass))
                {
                    update.Password = newPass.Trim();
                    db.SaveChanges();
                    TempData["isSuccessChange"] = true;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData["isFailChange"] = true;
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {
                TempData["isFailChange"] = true;
                  return RedirectToAction("Index", "Home");
            }
        }

        [AuthenticationFilter]
        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}