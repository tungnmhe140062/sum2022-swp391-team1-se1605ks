﻿using Project.Filter;
using Project.Models;
using Project.Models.manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
// order's status { 0: order placed, 1: processing, 2:shipping, 3: cancel, 4: complete }
namespace Project.Controllers
{
    public class SaleController : Controller
    {
        private ProjectEntities db = new ProjectEntities();
        public static string startDate = "";
        public static string endDate = "";
        // GET: Sale
        [AuthenticationFilter]
        [AuthorizationFilter(role: "sale;sale manager")]
        public ActionResult Index(int? status, DateTime? start, DateTime? end)
        {
            SaleCommonViewModel ovm = new SaleCommonViewModel();
            ovm.Title = "Dashboard";
            ovm.Tab = TabEnumSale.Dashboard;
            //set account
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                ovm.Account = selectAccount;
            }
            //set CountOrderSuccess
            ovm.CountOrderSuccess = CountOrderSuccess();
            ovm.CountOrder = CountTotalOrder();
            ovm.TotalSales = TotalSales();
            ovm.Last7Days = GetLast7Date(null);
            ovm.message = "";

            if (status == null)
            {
                ovm.status_name = "Complete";
                ovm.Last7DaysData = GetOrderData(4,null,null);
                ovm.status_id = 4;
            }else if(status != null && start==null && end == null)
            {
                ovm.status_name = GetStatusName((int)status);
                ovm.Last7DaysData = GetOrderData((int)status,null,null);
                ovm.status_id = (int)status;
            }else if (status != null && start !=null && end!=null)
            {
                ovm.status_id = (int)status;
                ovm.status_name = GetStatusName((int)status);
                if (Is7Days((DateTime)start, (DateTime)end) == true)
                {
                    ovm.Last7Days = GetLast7Date(end);
                    DateTime diff = (DateTime)end;
                    endDate = diff.ToString("yyyy-MM-dd");
                    
                    diff = diff.AddDays(-6);
                    startDate = diff.ToString("yyyy-MM-dd");
                }
                else
                {
                    ovm.Last7Days = GetLast7Date(null);
                    ovm.message = "Start - From must be 7 days";
                }
                ovm.Last7DaysData = GetOrderData((int)status, start, end);
            }
            ovm.startDate = startDate;
            ovm.endDate = endDate;
            return View(ovm);
        }

        public bool Is7Days(DateTime start, DateTime end)
        {
            DateTime diff = end.AddDays(-6);
            if(start == diff)
            {
                return true;
            }
            return false;
        }
        public string GetLast7Date(DateTime? end)
        {
            string result = "";
            List<DateTime> list = new List<DateTime>();
            DateTime today = new DateTime();
            if (end == null)
            {
                today = DateTime.Today;
            }
            else
            {
                today = (DateTime)end;
            }

            for (int i = 1; i <= 6; i++)
            {
                DateTime date_minus = today.AddDays(-i);
                list.Add(date_minus);
            }
            for (int i = list.Count() - 1; i >= 0; i--)
            {
                if (i == list.Count() - 1)
                {
                    startDate = list[i].ToString("yyyy-MM-dd");
                }
                result += "'" + list[i].ToString("MMM dd") + "'" + ", ";
            }
            endDate = today.ToString("yyyy-MM-dd");
            result += "'" + today.ToString("MMM dd") + "'";

            return result;
        }

        public string GetStatusName(int status)
        {
            string result = " ";
            switch (status)
            {
                case 0: result = "Order Placed"; break;
                case 1: result = "Processing"; break;
                case 2: result = "Shipping"; break;
                case 3: result = "Cancel";break;
                case 4: result = "Complete";break;
                default: result = "";break;
            }
            return result;
        }
        public string GetOrderData(int? status, DateTime? start_date, DateTime? end_date)
        {
            String data = "";
            DateTime today = new DateTime();
            TimeSpan st = new TimeSpan(00, 00, 00);
            TimeSpan et = new TimeSpan(23, 59, 59);

            //start = null; end = null
            if (start_date == null || end_date == null || status == null)
            {
                // neu null het - mac dinh lay status = complete = 4
                today = DateTime.Today;
                status = 4;
                for (int i = 6; i >= 0; i--)
                {
                    DateTime date_minus_start = today.AddDays(-i);
                    date_minus_start = date_minus_start.Date + st;

                    DateTime date_minus_end = today.AddDays(-i);
                    date_minus_end = date_minus_end.Date + et;

                    if (i == 6) startDate = date_minus_end.ToString("yyyy-MM-dd");
                    if (i == 0) endDate = date_minus_end.ToString("yyyy-MM-dd");
                    
                    var result = db.Orders.Where(x => x.CreatedAt >= date_minus_start && x.CreatedAt <= date_minus_end && x.Status == status).Count();
                    data += "'" + result + "' , ";
                }
                DateTime today_start = new DateTime();
                today_start = today_start.Date + st;
                DateTime today_end = new DateTime();
                today_end = today_end.Date + et;
                var result1 = db.Orders.Where(x => x.CreatedAt >= today_start && x.CreatedAt <= today_end && x.Status == status).Count();
                data += "'" + result1 + "' , ";
            }
            else if(start_date != null && end_date != null && status !=null)
            {
                DateTime start = (DateTime)start_date;
                DateTime end = (DateTime)end_date;

                //set start_date and end_date send to view
                startDate = start.ToString("yyyy-MM-dd");
                endDate = end.ToString("yyyy-MM-dd");

                DateTime check = end.AddDays(-6);
               // bool check = start <= end ? true : false; 
                if(Is7Days(start,end)==true)
                {
                    for(int i = 6; i >= 0; i--)
                    {
                        DateTime date_minus_start = end.AddDays(-i);
                        date_minus_start = date_minus_start.Date + st;

                        DateTime date_minus_end = end.AddDays(-i);
                        date_minus_end = date_minus_end.Date + et;
                        var result = db.Orders.Where(x => x.CreatedAt >= date_minus_start && x.CreatedAt <= date_minus_end && x.Status == (int)status).Count();
                        data += "'" + result + "' , ";
                    }
                    DateTime today_start = new DateTime();
                    today_start = end.Date + st;
                    DateTime today_end = new DateTime();
                    today_end = end.Date + et;
                    var result1 = db.Orders.Where(x => x.CreatedAt >= today_start && x.CreatedAt <= today_end && x.Status == (int)status).Count();
                    data += "'" + result1 + "' , ";
                }
                else
                {

                    return GetOrderData(status, null, null);
                }
            }
            else
            {
                return GetOrderData(status, null, null);
            }

            return data;
        }

        public int TotalSales()
        {
            int result = 0;
            var order = db.Orders.Where(x => x.Status == 4).ToList();
            foreach(var o in order)
            {
                var order_detail = db.OrderDetails.Where(x => x.OrderId == o.Id).ToList();
                int total_order_price = 0;
                if(order_detail != null)
                {
                    foreach(var od in order_detail)
                    {
                        total_order_price += od.SalePrice * od.Quantity;
                    }
                }
                result += total_order_price;
            }
            return result;
        }
        public int CountTotalOrder()
        {
            var result = db.Orders.Count();
            return result;
        }
        public int CountOrderSuccess()
        {
            var result = db.Orders.Where(x => x.Status == 4).Count();

            return result;
        }



    }
}