﻿using System;
using Project.Models;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;
using Project.Filter;

namespace Project.Controllers
{
    public class ProductController : Controller
    {

        // GET: Product
        private ProjectEntities db = new ProjectEntities();

      
        [HttpGet]
        public ActionResult ProductList(int? id, int? page,string search )
        {
            CommonViewLayout commonViewLayout = new CommonViewLayout();
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                commonViewLayout.Account = selectAccount;
            }         
            
            ProductViewModel viewModel = new ProductViewModel();

            viewModel.Categories = db.Categories.ToList();

            if (page == null || page == 1)
            {
                ViewBag.page = 1;
            }
            else ViewBag.page = page;
            ViewBag.searchz=search;
            ViewBag.cate = id;
            if (search != null)
            {
                if (id == null)
                {
                    if (page == null)
                    {
                        viewModel.ProductCategory = db.ProductCategories.Where(c => c.Product.Title.Contains(search)).OrderByDescending(c => c.CreatedAt).Take(12).ToList();
                    }
                    else
                    {
                        viewModel.ProductCategory = db.ProductCategories.Where(c => c.Product.Title.Contains(search)).OrderByDescending(c => c.CreatedAt).Skip((int)(page - 1) * 12).Take(12).ToList();
                    }
                    ViewBag.paging = db.ProductCategories.Where(c => c.Product.Title.Contains(search)).Count();

                }
                else if (id != null)
                {
                    if (page == null)
                    {
                        viewModel.ProductCategory = db.ProductCategories.Where(c => c.CategoryId == id && c.Product.Title.Contains(search)).OrderByDescending(c => c.CreatedAt).Take(12).ToList();
                    }
                    else
                    {
                        viewModel.ProductCategory = db.ProductCategories.Where(c => c.CategoryId == id && c.Product.Title.Contains(search)).OrderByDescending(c => c.CreatedAt).Skip((int)(page - 1) * 12).Take(12).ToList();
                    }
                    ViewBag.paging = db.ProductCategories.Where(c => c.CategoryId == id && c.Product.Title.Contains(search)).Count();

                }
            }
            else
            {
                if (id == null)
                {
                    if (page == null)
                    {
                        viewModel.ProductCategory = db.ProductCategories.OrderByDescending(c => c.CreatedAt).Take(12).ToList();
                    }
                    else
                    {
                        viewModel.ProductCategory = db.ProductCategories.OrderByDescending(c => c.CreatedAt).Skip((int)(page - 1) * 12).Take(12).ToList();
                    }
                    ViewBag.paging = db.ProductCategories.Count();

                }
                else if (id != null)
                {
                    if (page == null)
                    {
                        viewModel.ProductCategory = db.ProductCategories.OrderByDescending(c => c.CreatedAt).Take(12).ToList();
                    }
                    else
                    {
                        viewModel.ProductCategory = db.ProductCategories.OrderByDescending(c => c.CreatedAt).Skip((int)(page - 1) * 12).Take(12).ToList();
                    }
                    ViewBag.paging = db.ProductCategories.Where(c => c.CategoryId == id ).Count();

                }
            }
        
            
            viewModel.Products = db.Products.OrderByDescending(p => p.CreatedAt).ToList();
            commonViewLayout.Data = viewModel;
            
            
            return View(commonViewLayout);
        }
        public ActionResult ProductDetail(int? id)
        {
            CommonViewLayout commonViewLayout = new CommonViewLayout();
            ProductViewModel viewModel = new ProductViewModel();
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                commonViewLayout.Account = selectAccount;
            }
            viewModel.ProductCategory = db.ProductCategories.Where(c => c.ProductId == id).ToList();
            viewModel.ImageProducts = db.ImageProducts.Where(i => i.ProductId == id);
            viewModel.Products = db.Products.ToList();
            Session["ProductId"] = viewModel.ProductCategory.FirstOrDefault().ProductId;
        
            commonViewLayout.Data = viewModel;
            return View(commonViewLayout);
        }
       
        public ActionResult Search(string search)
        {
            ProductViewModel viewModel = new ProductViewModel();
            viewModel.Products = db.Products.OrderByDescending(p => p.CreatedAt).Where(p => p.Title.Contains(search)).ToList();
            
            return null;
        }
        [HttpPost]
        [AuthenticationFilter]
        [AuthorizationFilter(role: "customer")]
        public ActionResult AddToCart(int quantity)
        {
            var accId = Int32.Parse(Session["userId"].ToString());
            var prId = Int32.Parse(Session["ProductId"].ToString());
            var check = db.Carts.Where(c => c.AccountId == accId && c.ProductId == prId).FirstOrDefault();
            if(check == null)
            {
                Cart cart = new Cart();
                cart.Quantity = quantity;
                cart.ProductId = prId ;
                cart.AccountId = accId;
                db.Carts.Add(cart);
            }
            else
            {
                check.Quantity += quantity;
            }

            db.SaveChanges();
            return RedirectToAction("ProductList");
        }
        
        [AuthenticationFilter]
        [AuthorizationFilter(role: "customer")]
        public ActionResult AddToCart1(int id)
        {
            var accId = Int32.Parse(Session["userId"].ToString());
            var check = db.Carts.Where(c => c.AccountId == accId && c.ProductId == id).FirstOrDefault();
            if(check == null)
            {
                Cart cart = new Cart();
                cart.Quantity = 1;
                cart.ProductId = id;
                cart.AccountId = accId;
                db.Carts.Add(cart);
            }
            else
            {
                check.Quantity += 1;
            }
           
            db.SaveChanges();
            return RedirectToAction("ProductList");
        }



    }
}

