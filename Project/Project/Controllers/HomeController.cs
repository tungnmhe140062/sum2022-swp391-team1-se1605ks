using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Web.Mvc;
using Project.Models;
using System.Linq;
using Project.Filter;

namespace Project.Controllers
{
    public class  HomeViewLayout 
    {
        public Account Account { get; set; }
        public bool IsFailLogin { get; set; }
        public bool IsFailRegister { get; set; }
        public bool IsRegisterSuccess { get; set; }
        public bool IsNotActiveAccount { get; set; }
        public bool ActiveFail { get; set; }
        public bool ActiveSuccess { get; set; }
        public bool IsInvalidPasswordRegister { get; set; }
        public bool IsInvalidPhoneRegister { get; set; }
        public bool IsFailChange { get; set; }
        public bool IsSuccessChange { get; set; }
    }

    public class CommonViewLayout : HomeViewLayout
    {
        public ProductViewModel Data { get; set; }
    }



    public class HomeController : Controller
    {
        private ProjectEntities db = new ProjectEntities();

        public ActionResult Index()
        {
            HomeViewModel1 viewModel = new HomeViewModel1();

            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                viewModel.Account = selectAccount;
            }
            
            if (TempData["isFailChange"] != null && Boolean.Parse(TempData["isFailChange"].ToString()))
            {
                viewModel.IsFailChange = true;
            }
            if (TempData["isSuccessChange"] != null && Boolean.Parse(TempData["isSuccessChange"].ToString()))
            {
                viewModel.IsSuccessChange = true;
            }
            if (TempData["isLoginFail"] != null && Boolean.Parse(TempData["isLoginFail"].ToString()))
            {
                viewModel.IsFailLogin = true;
            }
            else if (TempData["isNotActiveAccount"] != null && Boolean.Parse(TempData["isNotActiveAccount"].ToString()))
            {
                viewModel.IsNotActiveAccount = true;
            }
            else if (TempData["isRegisterFail"] != null && Boolean.Parse(TempData["isRegisterFail"].ToString()))
            {
                viewModel.IsFailRegister = true;
            }
            else if (TempData["isRegisterSuccess"] != null && Boolean.Parse(TempData["isRegisterSuccess"].ToString()))
            {
                viewModel.IsRegisterSuccess = true;
            }
            else if (TempData["activeFail"] != null && Boolean.Parse(TempData["activeFail"].ToString()))
            {
                viewModel.ActiveFail = true;
            }
            else if (TempData["activeSuccess"] != null && Boolean.Parse(TempData["activeSuccess"].ToString()))
            {
                viewModel.ActiveSuccess = true;
            }
            else if (TempData["isInvalidPasswordRegister"] != null &&
                     Boolean.Parse(TempData["isInvalidPasswordRegister"].ToString()))
            {
                viewModel.IsInvalidPasswordRegister = true;
            }
            else if (TempData["isInvalidPhoneRegister"] != null &&
                     Boolean.Parse(TempData["isInvalidPhoneRegister"].ToString()))
            {
                viewModel.IsInvalidPhoneRegister = true;
            }

            viewModel.Products = db.Products.OrderByDescending(p => p.CreatedAt).ToList();

            viewModel.Categories = db.Categories.ToList();

            viewModel.NewBlog = db.Blogs.ToList()[0];

            viewModel.Slider = db.Sliders.Where(s => s.Status == true).ToList();
            
            return View(viewModel);
        }
    }
}