﻿using Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Project.Controllers
{
    

    public class ResetPasswordController : Controller
    {

        private ProjectEntities db = new ProjectEntities();
        ResetPasswordViewModel rpm = new ResetPasswordViewModel();
        // GET: ResetPassword
        public ActionResult Index()
        {
            
            Account a = null;
            rpm.Account = a;
            return View(rpm);
        }
        private string generateRandomPassword(int length)
        {
            const string VALID_PASS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random random = new Random();
            while (0 < length--)
            {
                res.Append(VALID_PASS[random.Next(VALID_PASS.Length)]);
            }

            return res.ToString();
        }
        [HttpPost]
        public ActionResult SendMail(string email)
        {
            var account = (Account) db.Accounts.Where(x => x.Email == email).FirstOrDefault();
            if (account != null)
            {
                string randomToken = generateRandomPassword(8);
                account.CodeForgotPassword = randomToken;
                db.SaveChanges();
                SendGridConfig.SendEmailResetPass(email, randomToken);
                rpm.messageSuccess = "Check your email";
                rpm.messageError = null;
            }
            else
            {
                rpm.messageError = "Email doesn't exist";
                rpm.messageSuccess = null;
            }
            return View("Index",rpm); 
        }

        [HttpPost]
        public ActionResult ChangePassword(string email, string token,string password, string repassword)
        {
            try
            {
                var update = db.Accounts.First(g => g.Email == email);
                string oldPass = update.Password;
                if (password.Equals(repassword) && !password.Equals(oldPass))
                {
                    update.Password = password.Trim();
                    db.SaveChanges();
                    TempData["isSuccessChange"] = true;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    string mess = "Password and Confirm password do not match <3";
                    return RedirectToAction("Forget", new { token=token, mess=mess  });
                }
            }
            catch (Exception)
            {
                string mess = "Password and Confirm password do not match <3";
                return RedirectToAction("Forget", new { token = token, mess = mess });
            }
        }
        public ActionResult Forget(string token)
        {
            Account a = null;
            rpm.Account = a;
            int expried_minutes = 2;
            if (token == null)
            {
                return RedirectToAction("Index");
            }
            var account = (Account)db.Accounts.Where(x => x.CodeForgotPassword == token).FirstOrDefault();
            if (account != null)
            {
                //check validate time token
                DateTime current_time = DateTime.Now;
                DateTime token_create_time = account.UpdatedAt;
                ViewBag.email = account.Email;
                var diffTime = current_time - token_create_time;
                if (diffTime.Minutes > expried_minutes)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            
            return View("Forget/Index",rpm);
        }
    }
}