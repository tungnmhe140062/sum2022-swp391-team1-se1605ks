using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Project.Filter;
using Project.Models;
using Project.Models.manage;

namespace Project.Controllers.manage
{
    [AuthenticationFilter]
    [AuthorizationFilter(role: "marketing")]
    public class ManageMarketingController : Controller
    {
        private ProjectEntities db = new ProjectEntities();
        
        public ActionResult Feedback(int page = 1, int sortBy = 5, string q = "")
        {
            FeedbackMarketingViewModel viewModel = new FeedbackMarketingViewModel();
            viewModel.Title = "Feedback List";
            viewModel.Tab = TabEnumMkt.Feedback;

            if (page <= 0)
                page = 1;

            IEnumerable<Feedback> search = new List<Feedback>();

            switch (sortBy)
            {
                case 1:
                    search =
                        (from a in db.Feedbacks
                            join f in db.OrderDetails
                                on a.Id equals f.FeedbackId
                            join p in db.Products 
                                on f.ProductId equals p.Id
                            where p.Title.Contains(q)
                            orderby p.Title
                            select a);
                    break;
                case 2:
                    search =
                        (from a in db.Feedbacks
                            join f in db.OrderDetails
                                on a.Id equals f.FeedbackId
                            join p in db.Products 
                                on f.ProductId equals p.Id
                            join od in db.Orders on f.OrderId equals od.Id 
                            where p.Title.Contains(q)
                            orderby od.FullName
                            select a);
                    break;
                case 3:
                    search =
                        (from a in db.Feedbacks
                            join f in db.OrderDetails
                                on a.Id equals f.FeedbackId
                            join p in db.Products 
                                on f.ProductId equals p.Id
                            where p.Title.Contains(q)
                            orderby a.RateStar
                            select a);
                    break;
                case 4:
                    search =
                        (from a in db.Feedbacks
                            join f in db.OrderDetails
                                on a.Id equals f.FeedbackId
                            join p in db.Products 
                                on f.ProductId equals p.Id
                            join od in db.Orders on f.OrderId equals od.Id 
                            where p.Title.Contains(q)
                            orderby od.Status
                            select a);
                    break;
                case 5:
                    search =
                        (from a in db.Feedbacks
                            join f in db.OrderDetails
                                on a.Id equals f.FeedbackId
                            join p in db.Products 
                                on f.ProductId equals p.Id
                            where p.Title.Contains(q)
                            orderby a.CreatedAt
                            select a);
                    break;
            }

            var listFeedbackTake = search.Skip((page - 1) * 7).Take(7).ToList();
            
            viewModel.ListFeedback = listFeedbackTake;
            viewModel.Page = page;
            viewModel.SortBy = sortBy;
            viewModel.Query = q;
            viewModel.TotalPage = search.ToList().Count / 7 + (search.ToList().Count % 7 == 0 ? 0 : 1);
            
            return View(viewModel);
        }

        public ActionResult SliderDetail(int id = 0)
        {
            if (id == 0)
                return RedirectToAction("Index", "Home");

            SliderDetailMarketingViewModel viewModel = new SliderDetailMarketingViewModel();
            viewModel.Title = "Slider detail";
            viewModel.Tab = TabEnumMkt.Slider;

            var selectSlider =
                (from slider in db.Sliders
                    where slider.Id == id
                    select slider).ToList();
            
            if (selectSlider.Count == 0)
                return RedirectToAction("Index", "Home");

            viewModel.SelectSlider = selectSlider[0];
            
            return View(viewModel);
        }

        public ActionResult AccountList(string name = "", string email = "", string phone = "", int page = 1, int sortBy = 1)
        {
            AccountListMarketingViewModel viewModel = new AccountListMarketingViewModel();
            viewModel.Title = "Customer list";
            viewModel.Tab = TabEnumMkt.Customer;
            
            if (page <= 0)
                page = 1;
            
            var selectList =
                db.Accounts
                    .Where(a => a.FullName.Contains(name))
                    .Where(a => a.Email.Contains(email))
                    .Where(a => a.Phone.Contains(phone))
                    .Where(a => a.Role == 0);
            
            // sort by name
            if (sortBy == 2)
                selectList = selectList.OrderBy(a => a.FullName);
            
            // sort by email
            if (sortBy == 3)
                selectList = selectList.OrderBy(a => a.Email);
            
            // sort by phone
            if (sortBy == 4)
                selectList = selectList.OrderBy(a => a.Phone);
            
            // sort by status
            if (sortBy == 5)
                selectList = selectList.OrderBy(a => a.IsActive);
            
            // sort by created at
            if (sortBy == 1)
                selectList = selectList.OrderBy(a => a.CreatedAt);

            viewModel.ListAccount = selectList.Skip((page - 1) * 7).Take(7).ToList();
            
            viewModel.QueryName = name;
            viewModel.QueryEmail = email;
            viewModel.QueryPhone = phone;
            viewModel.SortBy = sortBy;
            
            viewModel.TotalPage = selectList.ToList().Count / 7 + (selectList.ToList().Count % 7 == 0 ? 0 : 1);
            viewModel.Page = page;
            
            return View(viewModel);
        }
    }
}