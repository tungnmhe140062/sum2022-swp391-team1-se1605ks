﻿using Project.Filter;
using Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.Models.manage;

namespace Project.Controllers
{
    public class UserListModel : AdminCommonViewModel
    {
        public IEnumerable<Account> accounts { get; set; }
        public Account Account { get; set; }
    }
    public class UserListController : Controller
    {
        private ProjectEntities db = new ProjectEntities();
        // GET: UserList
        
        [AuthenticationFilter]
        [AuthorizationFilter(role: "admin")]
        public ActionResult UserList()
        {
            UserListModel userModel = new UserListModel();
            userModel.Title = "Manage User";
            userModel.Tab = TabEnumAdmin.Customer;
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                userModel.Account = selectAccount;
            }

            userModel.accounts = db.Accounts.ToList();


            return View(userModel);  
        }
        [AuthenticationFilter]
        [AuthorizationFilter(role: "admin")]
        public ActionResult UserDetail(int? id)
        {
            UserListModel userDetailModel = new UserListModel();

            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                userDetailModel.Account = selectAccount;
            }

            userDetailModel.accounts = db.Accounts.Where(a => a.Id == id);

            return View(userDetailModel);
        }
    }
}