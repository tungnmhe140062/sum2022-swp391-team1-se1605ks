using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Project.Filter;
using Project.Models;
using Project.Models.cart;

namespace Project.Controllers
{
    [AuthenticationFilter]
    [AuthorizationFilter(role: "customer")]
    public class CartController : Controller
    {
        private ProjectEntities db = new ProjectEntities();

        public ActionResult Index()
        {
            CartViewModel model = new CartViewModel();
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                model.Account = selectAccount;
            }

            // get list product in cart
            var selectCart = (from c in db.Carts where c.AccountId == model.Account.Id select c).ToList();
            model.Cart = selectCart;

            // Subtotal of cart
            double total = 0;
            foreach (var item in model.Cart)
            {
                total += item.Product.SalePrice * item.Quantity;
            }

            model.Total = total;

            return View(model);
        }

        [HttpPost]
        public ActionResult Increase(int id)
        {
            var selectCart = (from c in db.Carts where c.Id == id select c).ToList()[0];
            selectCart.Quantity++;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Decrease(int id)
        {
            var selectCart = (from c in db.Carts where c.Id == id select c).ToList()[0];
            if (selectCart.Quantity > 1)
            {
                selectCart.Quantity--;
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Remove(int id)
        {
            var selectCart = (from c in db.Carts where c.Id == id select c).ToList()[0];
            db.Carts.Remove(selectCart);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Contact()
        {
            CartContactViewModel layout = new CartContactViewModel();
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                layout.Account = selectAccount;
            }

            long subTotal = 0;

            (from dbCart in db.Carts where dbCart.AccountId == layout.Account.Id select dbCart).ToList()
                .ForEach(dbCart =>
                {
                    subTotal += dbCart.Quantity * dbCart.Product.SalePrice;
                });

            layout.SubTotal = subTotal;

            return View(layout);
        }

        [HttpPost]
        public ActionResult SubmitCartContact(
            string name,
            string email,
            string phone,
            string address,
            string note
        )
        {
            int userid = Int32.Parse(Session["userId"].ToString());
            
            name = name.Trim();
            email = email.Trim();
            phone = phone.Trim();
            address = address.Trim();
            note = note.Trim();

            Order newOrder = new Order()
            {
                AccountId = userid,
                FullName = name,
                Email = email,
                Phone = phone,
                Address = address,
                Note = note,
                Status = 0,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
            };

            var temp = db.Orders.Add(newOrder);
            db.SaveChanges();

            var listCart = (from e in db.Carts where e.AccountId == userid select e).ToList();

            List<OrderDetail> listOrderDetail = new List<OrderDetail>();
            
            foreach (var cart in listCart)
            {
                OrderDetail orderDetail = new OrderDetail()
                {
                    OrderId = temp.Id,
                    ProductId = cart.ProductId,
                    Quantity = cart.Quantity,
                    SalePrice = cart.Product.SalePrice,
                    OriginalPrice = cart.Product.OriginalPrice,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                };
                listOrderDetail.Add(orderDetail);
            }
            
            db.OrderDetails.AddRange(listOrderDetail);
            
            // delete row in db
            foreach (var cart in listCart)
            {
                db.Carts.Remove(cart);
            }

            db.SaveChanges();

            return RedirectToAction("Completion");
        }

        public ActionResult Completion()
        {
            HomeViewLayout layout = new HomeViewLayout();
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                layout.Account = selectAccount;
            }

            return View(layout);
        }

        public ActionResult MyOrder(string id)
        {
            MyOrderViewModel layout = new MyOrderViewModel();
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                layout.Account = selectAccount;
            }

            layout.IsEmpty = false;
            
            if (id == null)
            {
                var list = (from e in db.Orders where e.AccountId == layout.Account.Id select e).OrderByDescending(e => e.CreatedAt).ToList();
                if (list.Count == 0)
                {
                    layout.IsEmpty = true;
                    return View(layout);
                }

                return Redirect("/my-order?id=" + list[0].Id);
            }
            
            var listOrder = db.Orders.Where(e => e.AccountId == layout.Account.Id).OrderByDescending(e => e.CreatedAt).ToList();
            layout.Orders = listOrder;
            
            int orderId = Int32.Parse(id);
            layout.SelectedOrderId = orderId;
            
            var listOrderDetail = db.OrderDetails.Where(e => e.OrderId == orderId).ToList();
            layout.OrderDetails = listOrderDetail;
            
            var selectOrder = db.Orders.Where(e => e.Id == orderId).ToList()[0];
            layout.SelectOrder = selectOrder;

            return View(layout);
        }
    }
}