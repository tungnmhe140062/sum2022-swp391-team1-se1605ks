﻿using Project.Models;
using Project.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.Models.manage;
using System.IO;

namespace Project.Controllers
{
    public class SliderController : Controller
    {
        // GET: SliderMKT
        private int pageSize = 5;
        private ProjectEntities db = new ProjectEntities();
        [AuthenticationFilter]
        [AuthorizationFilter(role: "marketing;admin")]
        public ActionResult Index(int? page,string filter="")
        {
            SliderViewModel sld = new SliderViewModel();
            sld.Title = "Slider";
            sld.Tab = TabEnumMkt.Slider;
            int limit = pageSize;
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];

                sld.Account = selectAccount;
            }
            int total_page = CountSlider(filter);
            if(page == null)
            {
                sld.Slider = GetAllPagginated(1, limit, filter);
                sld.Current_Page = 1;
            }
            else if (page != null && page <= pageSize && page <= total_page)
            {
                sld.Slider = GetAllPagginated((int)page, limit,filter);
                sld.Current_Page = (int)page;

            }
            else if (page != null && page > pageSize && page <= total_page)
            {
                sld.Current_Page = 1;
                sld.Slider = GetAllPagginated(1, limit, filter);
                return Redirect("../Slider/Index");
            }
            
            sld.filter = filter;
            sld.CountList = total_page;
            return View(sld);
        }

        public int CountSlider(string filter)
        {
            int total = 0;
            
            if (filter != "")
            {
                bool status = filter == "active" ? true : false;
                total = (int)Math.Ceiling((double)db.Sliders.Where(x => x.Status == status).Count() / pageSize);
            }
            else
            {
                total = (int)Math.Ceiling((double)db.Sliders.Count() / pageSize);
            }
            return total;
        }

        public List<Slider> GetAllPagginated(int currentPage, int limit, string filter)
        {
            var result = (List<Slider>)null;
            SliderViewModel sld = new SliderViewModel();
            List<Slider> list = null;
            if(filter == "")
            {
                 result = db.Sliders.OrderBy(x => 0).Skip(limit * (currentPage - 1)).Take(limit).ToList();

            }
            else
            {
                bool status = filter == "active" ? true : false;
                result = db.Sliders.OrderBy(x => 0).Skip(limit * (currentPage - 1)).Where(x => x.Status == status).Take(limit).ToList();
            }
            
            if (result != null)
            {
                list = new List<Slider>();
                list = result;
            }
            return list;
        }

        [AuthenticationFilter]
        [AuthorizationFilter(role: "marketing;admin")]
        public ActionResult EditSlider(int? id)
        {
            SliderViewModel sld = new SliderViewModel();
            sld.Title = "Edit Slider";
            sld.Tab = TabEnumMkt.Slider;
            int limit = pageSize;
            if (Session["userId"] != null)
            {
                int userid = Int32.Parse(Session["userId"].ToString());
                var selectAccount = (from e in db.Accounts where e.Id == userid select e).ToList()[0];
                sld.Account = selectAccount;
            }
            if(id != null)
            {
                var result = (Slider)db.Sliders.Where(x => x.Id == id).FirstOrDefault();
                if (result != null)
                {
                    sld.slider = result;
                }
                else
                {
                    return RedirectToAction("Index","Slider");
                }
            }
            else
            {
                return RedirectToAction("Index", "Slider");
            }
            return View(sld);
        }
        
        [HttpPost]
        public ActionResult Edit(int id, string title, HttpPostedFileBase image, string backlink, string status )
        {
            bool check = status == "enable" ? true : false;
            var slider = (Slider)db.Sliders.Where(x => x.Id == id).FirstOrDefault();
            
            if (slider != null)
            {
                slider.Title = title;
                if (image != null)
                {
                    // save image to temp folder
                    string newFileName = DateTime.Now.Ticks + "_" + image.FileName;
                    string path = Path.Combine(Server.MapPath("~/TempImageUpload"), newFileName);
                    image.SaveAs(path);

                    // upload to cloudinary and get url of image
                    string[] url = CloudinaryConfig.Upload(new string[] { path });

                    // delete image from temp folder
                    System.IO.File.Delete(path);

                    slider.Image = url[0];
                }
                slider.BackLink = backlink;
                slider.Status = check;
                int flag = db.SaveChanges();
            }
            return RedirectToAction("Index", "Slider");
        }
    }
}